//
//  CustomCell.h

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell
@property(weak,nonatomic)   IBOutlet UIImageView *imgType;
@property (weak, nonatomic) IBOutlet UILabel *tagHex;
@property (weak, nonatomic) IBOutlet UILabel *tagEncoding;
@property (weak, nonatomic) IBOutlet UILabel *tagCount;
@property (weak, nonatomic) IBOutlet UILabel *tagRSSI;
@end

//
//  libFWupdate.h
//  libFWupdate
//
//  Created by User on 2015. 7. 21..
//  Copyright (c) 2015년 yonghwan. All rights reserved.
//

#import <Foundation/Foundation.h>

#define SRC_RSP_OK					0x70;
#define SRC_RSP_ERROR				0x71;
#define SRC_RSP_DATA_END			0x72;
#define SRC_RSP_UNKNOWN_CMD			0x73;

@interface libFWupdate : NSObject{
    Boolean isRFID;
    
    Boolean mHexImageValid;
    NSInteger MaxFlashSize;
    UInt32 mLowestSpecifiedAddr;
    UInt32 mHighestSpecifiedAddr;
    unsigned char mHexImage[128*1024];
    BOOL mAwaitCommandCode;
    int mCurrentPageIndex;
    
    Boolean mPageSpecified512[(128*1024)/512]; // pyh
    Boolean mPageSpecified1024[(128*1024)/1024];
    
    UInt32 mAppFWstartAddr;
    UInt32 mAppFWendAddr;
    
    
    unsigned char commandCodeFW[8];
    
    unsigned char mCommandCode;
    int mValidPagecount;
    UInt16 mGetHexImagePartialCRC;
    
    unsigned char mTransmitBuffer[1026];
    
    NSData* getPageData;
    int getPageIndex;
    Boolean getPageFlag;
    Boolean dataFlag;
}

/*/
 RFID or Barcode
 param
 isRfid : YES is RFID, NO is Barcode
 현제 YES : rfid
     NO  : test
/*/
-(void) setDevice:(BOOL) isRfid;




/*/
version check before update 
-param
  version : reader version :: rcp.getFWVersion
-return
  YES : if Hexfile version is reader FW version
  NO : if hexfile version is not reader FW version ( need firmware update )
/*/
-(BOOL) versionCheck:(NSString *)version;


/*/
 init
/*/
-(void) firmwareUpdateInit;


/*/
  hexfile (FW) open => image File
 -return
    yes : if succes open file
    no : if fail
/*/
-(BOOL) openHexFile;


/*/

/*/
-(NSData *) firmwareUpdate:(NSData *)fwData;






@end

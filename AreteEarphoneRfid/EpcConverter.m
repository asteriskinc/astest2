//  EpcConverter.m

#import "EpcConverter.h"
@implementation NSData (NSDataToBinaryString)
-(NSString*) bytesToBinaryString
{
    NSMutableString* str = [[NSMutableString alloc] init];
    unsigned char* ptr= (unsigned char*) [self bytes];
    for(int i = 0; i < self.length; i++)
    {
        switch(*ptr & 0xF0)
        {
            case 0x00: [str appendString:@"0000"]; break;
            case 0x10: [str appendString:@"0001"]; break;
            case 0x20: [str appendString:@"0010"]; break;
            case 0x30: [str appendString:@"0011"]; break;
            case 0x40: [str appendString:@"0100"]; break;
            case 0x50: [str appendString:@"0101"]; break;
            case 0x60: [str appendString:@"0110"]; break;
            case 0x70: [str appendString:@"0111"]; break;
            case 0x80: [str appendString:@"1000"]; break;
            case 0x90: [str appendString:@"1001"]; break;
            case 0xA0: [str appendString:@"1010"]; break;
            case 0xB0: [str appendString:@"1011"]; break;
            case 0xC0: [str appendString:@"1100"]; break;
            case 0xD0: [str appendString:@"1101"]; break;
            case 0xE0: [str appendString:@"1110"]; break;
            case 0xF0: [str appendString:@"1111"]; break;
        }
        
        switch(*ptr++ & 0x0F)
        {
            case 0x00: [str appendString:@"0000"]; break;
            case 0x01: [str appendString:@"0001"]; break;
            case 0x02: [str appendString:@"0010"]; break;
            case 0x03: [str appendString:@"0011"]; break;
            case 0x04: [str appendString:@"0100"]; break;
            case 0x05: [str appendString:@"0101"]; break;
            case 0x06: [str appendString:@"0110"]; break;
            case 0x07: [str appendString:@"0111"]; break;
            case 0x08: [str appendString:@"1000"]; break;
            case 0x09: [str appendString:@"1001"]; break;
            case 0x0A: [str appendString:@"1010"]; break;
            case 0x0B: [str appendString:@"1011"]; break;
            case 0x0C: [str appendString:@"1100"]; break;
            case 0x0D: [str appendString:@"1101"]; break;
            case 0x0E: [str appendString:@"1110"]; break;
            case 0x0F: [str appendString:@"1111"]; break;
        }
    }
    
    return str;
}
@end

@implementation NSString (NSStringToLong)
-(long) binStringToLong {
    long data = 0;
    int idx;
    
    for (idx = 0; idx < self.length; idx++)
    {
        NSRange range = NSMakeRange(idx, 1);
        NSString* binStr = [self substringWithRange:range];
        data |= ([binStr intValue]&0x01) << (self.length - idx - 1);
    }
    return data;
}
@end

@implementation NSString (LeftPadding)

-(NSString *) stringByPaddingLeftToLength:(NSUInteger) newLength withString:(NSString *) padString
{
    if ([self length] <= newLength)
        return [[@"" stringByPaddingToLength:newLength - [self length] withString:padString startingAtIndex:0] stringByAppendingString:self];
    else
        return [self copy];
}

@end

@implementation EpcConverter

#define ID_SGTIN96 (0x30)

+(NSString*) toTypeString:(NSInteger)type
{
    switch (type)
    {
        case HEX_STRING:
            return @"HEX";
        case ASCII:
            return @"ASCII";
        case SGTIN96:
            return @"SGTIN96";
        case EAN13:
            return @"EAN13";
        default:
            break;
    }    
    return nil;
}

+(int) getEpcStartDigit:(NSData*) data
{
    int digit = 2; // PC
    unsigned char* ptr= (unsigned char*) [data bytes];
    
    if((ptr[0] & 0x02) == 0x02)
	{
	    if((ptr[2] & 0x08) == 0x08)
	    {
            digit = 6; // PC + XPC_W1 + XPC_W2
	    }
	    else
	    {
            digit = 4; // PC + XPC_W1
	    }
	}	
	
	return digit;
}

+(NSString*) toHexString:(NSData*) data
{
    NSMutableString* str = [[NSMutableString alloc] init];
    unsigned char* ptr= (unsigned char*) [data bytes];
    for(int i = 0; i < data.length; i++)
        [str appendFormat:@"%02X", *ptr++ & 0xFF ];
    
    return str;
}

+(int) getPartition:(NSString*) bin
{
    NSRange range = NSMakeRange(3, 3);
    NSString* binStr = [bin substringWithRange:range];
    
    return  (int)[binStr binStringToLong];
}

+(int) getCompanyPrefixBitLen:(int) partition
{
    const int gCompanyPrefixBitLen[] = {40, 37, 34, 30, 27, 24, 20};
    
    if(partition < 0 || partition > 6)
	    return 0;
	
	return gCompanyPrefixBitLen[partition];
}

+(NSString*) getCompanyPrefix:(NSString*) bin
{
	int nPartition = [self getPartition:bin];
	int nCompanyPrefixLen = [self getCompanyPrefixBitLen:nPartition];
    NSRange range = NSMakeRange(6, nCompanyPrefixLen);
	NSString *binCompanyPrefix = [bin substringWithRange:range];
	long nCompanyPrefix = [binCompanyPrefix binStringToLong];
	int digitCompanyPrefixLen = 12 - nPartition;
    NSString *ret = [NSString stringWithFormat:@"%ld", nCompanyPrefix];
	return [ret stringByPaddingLeftToLength:digitCompanyPrefixLen
                                 withString:@"0"];
}

+(NSString*) getItemReference:(NSString*) bin
{
	int nPartition = [self getPartition:bin];
	int nCompanyPrefixLen = [self getCompanyPrefixBitLen:nPartition];
    int nItemReferenceLen = 44 - nCompanyPrefixLen;
    NSRange range = NSMakeRange(6 + nCompanyPrefixLen, nItemReferenceLen);
	NSString *binItemReference = [bin substringWithRange:range];
	long nItemReference = [binItemReference binStringToLong];
	int digitItemReferenceLen = 1 + nPartition;
	
    NSString *ret = [NSString stringWithFormat:@"%ld", nItemReference];
    
	return [ret stringByPaddingLeftToLength:digitItemReferenceLen
                                 withString:@"0"];
}

+(NSString*) getSerial:(NSString*) bin
{
	int nPartition = [self getPartition:bin];
	int nCompanyPrefixLen = [self getCompanyPrefixBitLen:nPartition];
	int nItemReferenceLen = 44 - nCompanyPrefixLen;
    NSRange range = NSMakeRange(6 + nCompanyPrefixLen + nItemReferenceLen, 38);
	NSString *binSerial = [bin substringWithRange:range];
	long nSerial = [binSerial binStringToLong];
	
    NSString *ret = [NSString stringWithFormat:@"%ld", nSerial];
    
	return ret;
}

+(NSString*) getFilter:(NSString*) bin
{
    NSRange range = NSMakeRange(0, 3);
	NSString *binFilter = [bin substringWithRange:range];
    
    long nFilt = [binFilter binStringToLong];
	
    NSString *ret = [NSString stringWithFormat:@"%ld", nFilt];
    
	return ret;
}

+(NSString*) toSGTIN96:(NSData*) data
{
	NSString *bin = [self toBinString:data];
    
	if (bin.length == 0)
	    return @"NON-SGTIN96";
	else
    {
        NSMutableString* str = [[NSMutableString alloc] init];
        [str appendString:[self getFilter:bin]];
        [str appendString:@"."];
        [str appendString:[self getCompanyPrefix:bin]];
        [str appendString:@"."];
        [str appendString:[self getItemReference:bin]];
        [str appendString:@"."];
        [str appendString:[self getSerial:bin]];
        return str;
    }
}

+(NSString*) toEAN13:(NSData*) data
{
	NSString *bin = [self toBinString:data];
	if (bin.length == 0)
	    return @"NON-SGTIN96";
	else
	{
        NSMutableString* str = [[NSMutableString alloc] init];
        [str appendString:[self getCompanyPrefix:bin]];
        [str appendString:[[self getItemReference:bin] substringFromIndex:1]];
	    return [self appendParity:str];
	}
}



+(NSString*) toString:(NSInteger)type data:(NSData*) data
{
	if (type == ASCII)
	{
	    return [self toAscii:data];
	}
	else if(type == SGTIN96)
	{
	    return [self toSGTIN96:data];
	}
	else if(type == EAN13)
	{
	    return [self toEAN13:data];
	}
	else
	{
	    return [self toHexString:data];
	}
}

+(NSString*) toAscii:(NSData*) data
{
    
    //NSMutableString* str = [[NSMutableString alloc] init];
    int len = (int)data.length - (int)[self getEpcStartDigit:data];
    
    char* ptr= (char*) [data bytes];
    char newArray[len];
    //NSMutableData *newData = [[NSMutableData alloc] init];
    
    int idx = 0;
    for(int i = [self getEpcStartDigit:data]; i < data.length; i++)
    {
        char temp = ptr[i];
        if(temp > 0 && temp < 127)
        {
            newArray[idx++] = temp;
        }
        else
        {
            newArray[idx++] = ' ';
        }
    }
    
    NSData *newData = [[NSData alloc] initWithBytes:newArray length:idx];
    NSString *str = [[NSString alloc]
                       initWithData:newData
                       encoding:NSASCIIStringEncoding];
    
    return str;
}

+(NSString*) toBinString:(NSData*) data
{
	if(data.length < 14)
	    return @"";
	
	int startDigit = [self getEpcStartDigit:data];
    
	if(data.length < 12 + startDigit)
	    return @"";
	
    unsigned char* ptr= (unsigned char*) [data bytes];
	if(ptr[startDigit] != ID_SGTIN96) // PC (2 bytes) + EPC
	    return @"";
    
    NSData *newData = [data subdataWithRange:
                       NSMakeRange(startDigit + 1, data.length - startDigit - 1)];
    
	return [newData bytesToBinaryString];
}



+(NSString*) appendParity:(NSString*) message
{
    int sum = 0;
    int parity = 0;
    const int gWeighting[] = { 1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3 };
    
    for(int i = 0; i < 12; i++)
    {
        NSRange range = NSMakeRange(i, 1);
        NSString* str = [message substringWithRange:range];
        sum += [str intValue] * gWeighting[i];
    }
    
    parity = 10 - (sum % 10);
    if (parity == 10)
    {
        parity = 0;
    }
    
    return [message stringByAppendingString:[NSString stringWithFormat:@"%d", parity]];
}
@end



//
//  AntiColViewController.m
//  Created by koda on 2015/02/20.
//
//

#import "AntiColViewController.h"
#import "ComboDevices.h"
#import "ModelSettings.h"
@interface AntiColViewController ()<UITableViewDataSource,UITableViewDelegate,RcpRFIDDelegate>
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (strong,nonatomic) NSArray *modeList;
@property (assign,nonatomic) NSInteger selectedMode;
@property (assign,nonatomic) NSInteger selectedCounet;
@property (nonatomic,retain)ComboRFIDApi       *m_RFIDDevice;


@end

@implementation AntiColViewController


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.m_RFIDDevice    = [ComboRFIDApi sharedInstance];
    
    ModelSettings *model = [ModelSettings sharedInstance];
    _selectedMode =  model.m_RFIDDeviceInfo.m_nAntiMode;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];    
    self.modeList = @[@"MODE 0",
                      @"MODE 1",
                      @"MODE 2",
                      @"MODE 3",
                      @"MODE 4",
                      @"MODE 5",
                      @"MODE 6"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    #define DEVICE_ASX_300R      302
    CommonReaderInfo *info = [CommonReaderInfo sharedInstance];
    if(info.m_nReaderType   == DEVICE_ASX_300R)
        return 7;
    else
        return 5;
    
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    cell.textLabel.text = _modeList[indexPath.row];
    if (_selectedMode == indexPath.row)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    for (int i = 0; i < _modeList.count; i++)
    {
        UITableViewCell *cell = [_myTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
        if (i == indexPath.row)
        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            _selectedMode = indexPath.row;
        }
        else
        {
             cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
}

- (IBAction)done:(id)sender
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        
        int nDefultCounter = 1;

        BOOL success     = [self.m_RFIDDevice setAnticollision:_selectedMode Counter:nDefultCounter];
        UIAlertView *alert   = nil;
        NSString    *message = nil;

        if (!success)
        {
            message = @"Error";
            alert = [[UIAlertView alloc]initWithTitle:nil message:message delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
        }
        
    });
}




@end

//  EncodingType.m

#import "EpcConverter.h"
#import "EncodingTypeController.h"
#import "ModelSettings.h"
@interface EncodingTypeController ()
{
    NSIndexPath *_selectedIndexPath;
}
@property (nonatomic,retain)ModelSettings *m_Model;
@property (nonatomic,retain)NSMutableArray *typeArray;
@property (nonatomic,assign)NSInteger type;
@end

@implementation EncodingTypeController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (NSMutableArray *)typeArray
{
    if(!_typeArray)
    {
        _typeArray=   [[NSMutableArray alloc]init];//[NSMutableArray arrayWithObjects:nil];
        
        NSString *strType;
        
        int i = 0;
        while(YES)
        {
            strType = [EpcConverter toTypeString:i++];
            if(strType == nil)
                break;
            [_typeArray addObject:strType];
        }
    }
    
    return _typeArray;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _selectedIndexPath = nil;
    self.m_Model = [ModelSettings sharedInstance];
    self.type = [[NSUserDefaults standardUserDefaults] integerForKey:@"RFIDEncoding"];
}

- (IBAction)rightBarButtonItemClicked:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setInteger:(int)self.type forKey:@"RFIDEncoding"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.typeArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = [EpcConverter toTypeString:indexPath.row];
    if(self.type == indexPath.row)
    {
         cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
         cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}


#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedIndexPath = indexPath;
    
    self.type = indexPath.row;
    [self.tableView reloadData];
}


@end

//  SettingsFhLbtController.m

#import "SettingsFhLbtController.h"
#import "ModelSettings.h"

/*SDK header files*/
#import "ComboDevices.h"

@interface SettingsFhLbtController ()
@property (nonatomic,retain)ModelSettings *m_Model;
@end

@implementation SettingsFhLbtController
@synthesize senseTime;
@synthesize lbtLevel;
@synthesize lbtEnable;
@synthesize fhEnable;
@synthesize cwEnable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.m_Model = [ModelSettings sharedInstance];
    CommonReaderInfo *info = [CommonReaderInfo sharedInstance];
    CommonDevice *device = [CommonDevice sharedInstance];
    device.delegateRFID = self;
    self.olOnTime.text  = [NSString stringWithFormat:@"%d",info.nRFIDonTime];
    self.olOffTime.text = [NSString stringWithFormat:@"%d",info.nRFIDoffTime];
}




- (IBAction)rightBarButtonItemClicked:(id)sender
{    
    [self.olOnTime resignFirstResponder];
    [self.olOffTime resignFirstResponder];
    
    int nSetOnTime  = (int)[self.olOnTime.text integerValue];
    int nSetOffTime = (int)[self.olOffTime.text integerValue];
    
    CommonReaderInfo *info = [CommonReaderInfo sharedInstance];
    
    
    [[ComboRFIDApi sharedInstance] setFhLbtParam:(uint16_t)nSetOnTime
                   idleTime:(uint16_t)nSetOffTime
           carrierSenseTime:(uint16_t)info.nCst
                    rfLevel:(uint16_t)info.nRfl
           frequencyHopping:(uint8_t)info.nFh
           listenBeforeTalk:(uint8_t)info.nLbt
             continuousWave:(uint8_t)info.nCw];
}

-(void) showAlert:(NSString*)strMsg
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       
                       UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                       
                       [alert setMessage:strMsg];
                       [alert show];

                       
                   });

}

- (void)didSetFhLbt:(uint8_t)status
{
    if(status == 0x00)
    {
        [self showAlert:@"On/OFF Time Changed"];
    }
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)viewDidUnload {
    [self setOlOnTime:nil];
    [self setOlOffTime:nil];
    [super viewDidUnload];
}





@end

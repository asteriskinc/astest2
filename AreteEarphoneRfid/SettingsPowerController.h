//  SettingsPowerController.h

#import <UIKit/UIKit.h>
#import "protocols.h"
@interface SettingsPowerController : UITableViewController<RcpRFIDDelegate>
@property (nonatomic,retain)NSMutableArray *powerArray;
@property (nonatomic,assign)NSInteger initPower;
- (IBAction)rightBarButtonItemClicked:(id)sender;
@end




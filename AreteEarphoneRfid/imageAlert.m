//
//  Indicator.m
//  hitejinro
//

#import "imageAlert.h"

@implementation imageAlert

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


+(void)show:(BOOL)on target:(UIViewController*)viewController
{
    static dispatch_once_t pred = 0;
    
    __strong static UIImageView *background = nil;
    __strong static UIImageView *imageView1 = nil;
    __strong static UIImageView *imageView2 = nil;
    __strong static UILabel *loadingLabel = nil;
    
    dispatch_once(&pred,
                  ^{
                      background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 230 , 230)];
                      imageView1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 230 , 230)];
                      imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 230, 230)];
                      loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
                      
                  });
    
    if(imageView1 != nil)
    {
        background.frame = viewController.view.frame;
        background.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.8];
        
        NSString *path = [[NSString alloc] initWithString:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"rfidAlert.png"]];
        UIImage *bkgImg = [[UIImage alloc] initWithContentsOfFile:path];
        [imageView1 setImage:bkgImg];
        imageView1.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
        imageView1.contentMode = UIViewContentModeScaleAspectFit;
        [imageView1 setCenter:CGPointMake(viewController.view.frame.size.width/2, 130)];
        
        
        path = [[NSString alloc] initWithString:[[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"rfidAlertX.png"]];
        bkgImg = [[UIImage alloc] initWithContentsOfFile:path];
        [imageView2 setImage:bkgImg];
        imageView2.backgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:1];
        imageView2.contentMode = UIViewContentModeScaleAspectFit;
        [imageView2 setCenter:CGPointMake(imageView1.center.x, imageView1.center.y+230)];
        
        if(on == YES)
        {
            [viewController.view addSubview:background];
            [viewController.view addSubview:imageView1];
            [viewController.view addSubview:imageView2];
            background.hidden = NO;
            imageView1.hidden = NO;
            imageView2.hidden = NO;
        }
        else
        {
            background.hidden = YES;
            imageView1.hidden = YES;
            imageView2.hidden = YES;
        }
    }
}

@end

//  SettingsPowerController.m

#import "SettingsPowerController.h"
#import "ModelSettings.h"

/*SDK header files*/
#import "ComboDevices.h"

@interface SettingsPowerController ()
{
    NSIndexPath *_selectedIndexPath;
    int16_t powerLevel;
}
@end

@implementation SettingsPowerController
@synthesize initPower;

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated
{
    CommonReaderInfo *info = [CommonReaderInfo sharedInstance];
    powerLevel =   (int)info.fRFIDpower;
    
    
    CommonDevice *device = [CommonDevice sharedInstance];
    device.delegateRFID    = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if(!_powerArray)
    {
        _powerArray = [[NSMutableArray alloc]init];
    }
    
    CommonReaderInfo *info = [CommonReaderInfo sharedInstance];
   
     NSLog(@"[POWER] Now %f  Min %f  Max %f ",info.fRFIDpower,info.fRFIDpowerMin,info.fRFIDpowerMax);
    
    
    int nMin  = (int)info.fRFIDpowerMin;
    int nMAX  = (int)info.fRFIDpowerMax;
    
    
     NSLog(@"[POWER Nu] Now %d  Min %d  Max %d ",(int)info.fRFIDpower,nMin,nMAX);
    
    for(int i = nMin; i <= nMAX; i = i + 5)
    {
        [self.powerArray addObject:[NSNumber numberWithInt:i]];
    }
    [self.tableView reloadData];
    
}

-(void) showAlert:(NSString*)strMsg
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:strMsg delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                       
                       [alert setMessage:strMsg];
                       [alert show];
                   });
}


- (IBAction)rightBarButtonItemClicked:(id)sender
{
    
    ComboRFIDApi *device = [ComboRFIDApi sharedInstance];
    [device setOutputPowerLevel:powerLevel];
    
}

- (void)didSetOutputPowerLevel:(uint8_t)status
{
    if(status == 0x00) //Sucess
    {
        [self showAlert:@"Complete TX Power Set"];
    }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int nCount = (int)self.powerArray.count;
    return nCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    int ipower = [[self.powerArray objectAtIndex:indexPath.row] intValue];
  
    float power = (float) ipower / 10.0;
    cell.textLabel.text = [NSString stringWithFormat:@"%02.1f", power];
    
    if(powerLevel == ipower)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
  
    
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    powerLevel  = [[self.powerArray objectAtIndex:indexPath.row] intValue];
     NSLog(@"select   %d ",powerLevel);
    [self.tableView reloadData];
}


@end
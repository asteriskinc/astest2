//  LockController.h

#import <UIKit/UIKit.h>
#import "protocols.h"
@interface LockController : UIViewController <RcpRFIDDelegate>


//@property (nonatomic, strong, readwrite) NSString *epc;
@property (weak, nonatomic) IBOutlet UITextField *olTargetEpc;
@property (weak, nonatomic) IBOutlet UITextField *olAccessPassword;
@property (weak, nonatomic) IBOutlet UISegmentedControl *olTargetMemory;
@property (weak, nonatomic) IBOutlet UISegmentedControl *olAction;
- (IBAction)rightBarButtonItemClicked:(id)sender;
@end

//  PhyUtility.m

#import "PhyUtility.h"

@implementation PhyUtility
+(NSString*)getEpcStringFromPcEPCData:(NSData*)pcEpcData
{
    #define PC_XI                       0x0200
    #define XPC_XEB                     0x8000
    
    NSMutableString* tag = [[NSMutableString alloc]init] ;
    unsigned char* ptr= (unsigned char*) [pcEpcData bytes];
    
    
    for(int i = 0; i < pcEpcData.length; i++)
        [tag appendFormat:@"%02x", *ptr++ & 0xFF ];
    
    NSString *strEpc = @"";
    
    
    NSString *pcEpc     = tag;
    unsigned int pc;
    [[NSScanner scannerWithString:[pcEpc substringToIndex:4]] scanHexInt:&pc];
    
    if(pc & PC_XI)
    {    // check the XI bit on the PC
        unsigned int xpc;
        [[NSScanner scannerWithString:[pcEpc substringWithRange:NSMakeRange(4, 4)]] scanHexInt:&xpc];
        
        if (xpc & XPC_XEB)
        {     // check the XEB bit on the XPC_W1
            strEpc = [pcEpc substringFromIndex:12];
        }
        else
        {
            strEpc = [pcEpc substringFromIndex:8];
        }
    }
    else
    {
        strEpc = [pcEpc substringFromIndex:4];
    }
    return strEpc;
}

@end

@implementation NSString (NSStringHexToBytes)
-(NSData*) hexStringToBytes {
    NSMutableData* data = [NSMutableData data];
    int idx;
    for (idx = 0; idx+2 <= self.length; idx+=2) {
        NSRange range = NSMakeRange(idx, 2);
        NSString* hexStr = [self substringWithRange:range];
        NSScanner* scanner = [NSScanner scannerWithString:hexStr];
        unsigned int intValue;
        [scanner scanHexInt:&intValue];
        [data appendBytes:&intValue length:1];
    }
    return data;
}
@end

@implementation NSString (NSStringToHex)
-(NSData*) stringToHex {
    NSMutableData* data = [NSMutableData data];
    int idx;
    for (idx = 0; idx < self.length; idx++) {
        NSRange range = NSMakeRange(idx, 2);
        NSString* hexStr = [self substringWithRange:range];
        
        NSScanner* scanner = [NSScanner scannerWithString:hexStr];
        unsigned int intValue;
        [scanner scanHexInt:&intValue];
       
        [data appendBytes:&intValue length:1];
    }
    return data;
}
@end





@implementation NSData (NSDataToHexString)
-(NSString*) bytesToHexString 
{
    
    NSMutableString* str = [[NSMutableString alloc] init];
    unsigned char* ptr= (unsigned char*) [self bytes];
    
    //*/
    for(int i = 0; i < self.length; i++)
        [str appendFormat:@"%02X", *ptr++ & 0xFF ];
    
    /*/
    NSLog(@"bytesToHexString len : %lu",(unsigned long)self.length);
    
    for(int i = 0; i < self.length; i++){
        
        [str appendFormat:@"%c", *ptr++];
    }
    //*/
    return str;
}
@end

@implementation UIApplication (Logging)
+ (void) redirectConsoleLogToDocumentFolder:(NSString*)filename
{
    NSArray *paths
        = NSSearchPathForDirectoriesInDomains
            (NSDocumentDirectory,
             NSUserDomainMask, YES);
    
    NSString *documentsDirectory
        = [paths objectAtIndex:0];
    
    NSString *logPath
        = [documentsDirectory stringByAppendingPathComponent:filename];
    
    freopen([logPath cStringUsingEncoding:NSASCIIStringEncoding], "w",stderr);
}
@end

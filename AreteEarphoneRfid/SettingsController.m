//  SettingsController.m

#import "SettingsController.h"
#import "SettingsPopController.h"
#import "AboutController.h"
#import "CommonReaderInfo.h"

@interface SettingsController ()
@property(nonatomic, strong, readwrite) SettingsPopController *settingsPopController;
@property(nonatomic, strong, readwrite) AboutController *aboutController;
@property(nonatomic, retain) UIBarButtonItem *backBarButtonItem;

@property(nonatomic, retain) IBOutlet UITableViewCell *olCellRFID;
@end

@implementation SettingsController
@synthesize settingsPopController = _settingsPopController;
@synthesize aboutController       = _aboutController;


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"seguePopSettings"])
    {
       _settingsPopController = segue.destinationViewController;
    }
    else if ([segue.identifier isEqualToString:@"segueAbout"])
    {
        _aboutController      = segue.destinationViewController;
    }
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    CommonReaderInfo *info = [CommonReaderInfo sharedInstance];
    if(info.m_nCurrentSelectDevice != RCP_COMBO_DEVICE_RFID)
    [self.olCellRFID setHidden:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}
@end

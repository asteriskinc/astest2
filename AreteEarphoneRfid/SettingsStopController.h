//  SettingsStopController.h

#import <UIKit/UIKit.h>
@interface SettingsStopController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *olStopTagCount;
@property (weak, nonatomic) IBOutlet UITextField *olStopTime;
@property (weak, nonatomic) IBOutlet UITextField *olStopCycle;
- (IBAction)rightBarButtonItemClicked:(id)sender;
@end

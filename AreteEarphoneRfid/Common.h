//
//  Common.h
//  AsKey
//
//  Created by niwatako on 2015/09/25.
//  Copyright (c) 2015年 asterisk. All rights reserved.
//

#ifndef AsKey_Common_h
#define AsKey_Common_h

#define NSLOGGER_BUILD_USERNAME SPS_ROBIN_ASKS


#ifdef DEBUG
    #define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
    #define DLog(...)
#endif

#endif // #ifndef AsKey_Common_h

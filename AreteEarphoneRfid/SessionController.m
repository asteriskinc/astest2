//  SessionController.m

#import "SessionController.h"


/*SDK header files*/
#import "ComboDevices.h"

@interface SessionController ()
@property (nonatomic,assign)NSInteger session;
@property (nonatomic,retain)ComboRFIDApi *m_RFIDDevice;
@end

@implementation SessionController


- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.session = -1;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    CommonDevice *device = [CommonDevice sharedInstance];
    device.delegateRFID =self;
    self.m_RFIDDevice    = [ComboRFIDApi sharedInstance];
    [self.m_RFIDDevice getSession];
    

}

- (void)didSetSession:(uint8_t)status
{
    if(status == 0x00)
    {
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       
                       UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Changed Session" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                       
                       
                       [alert show];
                       
                       
                   });
    }
}



- (IBAction)rightBarButtonItemClicked:(id)sender
{
    if(self.session == -1)
    {
        [self.m_RFIDDevice  setSession:0];
        self.session = 0;
    }
    else
    {
        [self.m_RFIDDevice  setSession:self.session];
    }
    NSString *strMessage = [NSString stringWithFormat:@"%@  : %d",@"Set Session",(int)self.session ];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"1)Update Registry \n2) Reset H/W"
                                                    message:strMessage
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       [alert show];
                   });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"S%d", (int)indexPath.row];
    
    if(self.session == indexPath.row)
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    else
        cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.session = indexPath.row;
    
    [self.tableView reloadData];
}

-(void) sessionReceived:(uint8_t)session
{
    
    self.session = session;
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                        [self.tableView reloadData];
                   });
}

@end

//  SessionController.h

#import <UIKit/UIKit.h>
#import "protocols.h"

@interface SessionController : UITableViewController <RcpRFIDDelegate>
- (IBAction)rightBarButtonItemClicked:(id)sender;
@end

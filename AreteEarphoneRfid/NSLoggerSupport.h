//
//  NSLoggerSupport.h
//  AsKey
//
//  Created by niwatako on 2015/10/05.
//  Copyright (c) 2015年 asterisk. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSLoggerSupport : NSObject

+ (void)startLogger;

@end

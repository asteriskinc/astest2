//  RFIDFuntionsList.m
//  Created by Robin on 2016. 5. 16..

#import "RFIDFuntionsList.h"
#import "ModelSettings.h"
#import "GlobalDefine.h"
#import "Indicator.h"
#import "imageAlert.h"

/*SDK header files*/
#import "ComboDevices.h"

@interface RFIDFuntionsList()
{
}
@property(nonatomic, retain)  ModelSettings *m_Model;
@property(nonatomic, retain)  ComboRFIDApi       *m_RFIDDevice;
@property(nonatomic, retain)  CommonDevice  *m_CommonDevice;
@property (weak, nonatomic) IBOutlet UILabel *olPowerLever;
@property (weak, nonatomic) IBOutlet UILabel *olStopCondition;
@property (weak, nonatomic) IBOutlet UILabel *olOnOffTime;
@property (weak, nonatomic) IBOutlet UILabel *olRFChannel;
@property (weak, nonatomic) IBOutlet UILabel *olEncording;

- (IBAction)actionRSSISwitch:(UISwitch *)sender;
- (IBAction)btnSaveRegistry:(id)sender;
- (IBAction)actionHoppingOnOff:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch           *olSwitchRSSIOn;
@property (weak, nonatomic) IBOutlet UISwitch           *olHopping;
@property (weak, nonatomic) IBOutlet UILabel *olAntiCollisionMode;
@end




@implementation RFIDFuntionsList

-(void)viewDidLoad
{
    [super viewDidLoad];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.m_Model             = [ModelSettings sharedInstance];
    self.m_RFIDDevice        = [ComboRFIDApi sharedInstance];
    self.m_CommonDevice      = [CommonDevice sharedInstance];
    
    self.m_CommonDevice.delegateCommon = self;
    self.m_RFIDDevice.delegateRFID =self;
    
    int nEncording = (int)[[NSUserDefaults standardUserDefaults]integerForKey:@"RFIDEncoding"];
    
    switch (nEncording)
    {
        case df_ENCODINGTYPE_HEX:
            self.olEncording.text       = @"HEX";
            break;
        case df_ENCODINGTYPE_ASCII:
            self.olEncording.text       = @"ASCII";
            break;
        case df_ENCODINGTYPE_EAN13:
            self.olEncording.text       = @"EAN13";
            break;
        case df_ENCODINGTYPE_SGTIN96:
            self.olEncording.text       = @"SGTIN96";
            break;
        default:
            self.olEncording.text       = @"Unknow";
            break;
    }
    
    self.olPowerLever.text              = @"---";
    self.olStopCondition.text           = @"---";
    self.olOnOffTime.text               = @"---";
    self.olRFChannel.text               = @"---";
    self.m_CommonDevice.delegateRFID   = self;
    
    
    BOOL isRSSIOn =[[NSUserDefaults standardUserDefaults] boolForKey:@"RSSIOn"];
    
    [self.olSwitchRSSIOn setOn:isRSSIOn];
    
    
    int nTagCount =(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"RFIDScanTagCount"];
    int nScanTime =(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"RFIDScanTagTime"];
    int nCycle    =(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"RFIDScanTagInventory"];
    
    
    dispatch_async(dispatch_get_main_queue(),^{
        self.olStopCondition.text  =  [NSString stringWithFormat:@"%d/%d/%d",
                                       nTagCount,
                                       nScanTime,
                                       nCycle];}
                   );
    
    //Power
    [self performSelector:@selector(onTickRFIDPowerFuntion)     withObject:nil afterDelay:.2f];
    
    //Stop
    [self performSelector:@selector(onTickStopConditionFuntion) withObject:nil afterDelay:.4f];
    
    //OnOffTime
    [self performSelector:@selector(onTickOnOffTimeFuntion)     withObject:nil afterDelay:.6f];
    
    //Channel
    [self performSelector:@selector(onTickOnChannelFuntion)     withObject:nil afterDelay:.8f];
    
    //Hopping
    [self performSelector:@selector(onTickOnHoppingFuntion)     withObject:nil afterDelay:1.0f];
    
    //AntiCollison
    [self performSelector:@selector(onTickAnticollisionFuntion)     withObject:nil afterDelay:1.2f];
    
}


- (void) onTickAnticollisionFuntion
{
    [self.m_RFIDDevice getAnticollision];
}

- (void) onTickStopConditionFuntion
{
    [self.m_RFIDDevice getStopCondition];
}

- (void) onTickRFIDPowerFuntion
{
   [self.m_RFIDDevice getOutputPowerLevel];
}

- (void) onTickOnOffTimeFuntion
{
    [self.m_RFIDDevice getRFIDOnOffTime];
   
}

- (void) onTickOnChannelFuntion
{
    [self.m_RFIDDevice getChannel];
}

- (IBAction)btnSaveRegistry:(id)sender
{
    
    //Sotre data into Registry
     [self.m_RFIDDevice updateRegistry];
}

- (void)updatedRegistry:(uint8_t)status
{
    
    NSString *strResult = @"";
    
    //Sucess
    if(status == 0x00)
    {
        strResult = @"Sucess Data Store into Registry \nPlease Reset H/W";
    }
    else
    {
          strResult = @"Fail Store into Registry";
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Update Registry Result"
                                                    message:strResult
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       [alert show];
                   });
    
}




- (IBAction)actionRSSISwitch:(UISwitch *)sender
{
    UISwitch *sw = (UISwitch*)sender;
   
    [[NSUserDefaults standardUserDefaults] setBool:sw.isOn forKey:@"RSSIOn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
}




#pragma mark - CommonDelegate
//Reader (RFID Module Info)

/***********************************************************************/
#pragma mark Hopping
/***********************************************************************/

/*RFID ON/OFF Time Received*/
- (void)rfidOnOffTimeReceived:(NSData*)data
{
    CommonReaderInfo *info = [CommonReaderInfo sharedInstance];
    NSLog(@"[ONOFF] ON Time %d",info.nRFIDonTime);
    NSLog(@"[ONOFF] OFF Time %d",info.nRFIDoffTime);
    NSLog(@"[ONOFF] nCst %d",info.nCst);
    NSLog(@"[ONOFF] nRfl %d",info.nRfl);
    NSLog(@"[ONOFF] nLbt %d",info.nLbt);
    NSLog(@"[ONOFF] nCw %d",info.nCw);
    NSLog(@"[ONOFF] PH %d",info.nFh);
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                        self.olOnOffTime.text     =  [NSString stringWithFormat:@"%d/%d",info.nRFIDonTime,info.nRFIDoffTime];
                   });
   
    
    
}


- (void) onTickOnHoppingFuntion
{
    //Request Hopping Mode
    [self.m_RFIDDevice GetFrequencyHoppingMode];

}


//Smart Hoping On/OFF
-(void)resSmartHopingStatus:(uint8_t)status{
    
    if (status == 0x00)
    {
        [self.m_RFIDDevice updateRegistry];
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           [Indicator show:NO target:self];
                       });
    }
}


- (void) alertView:(UIAlertView *)selectHPMode clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch ([selectHPMode tag])
    {
            //Hopping  Mode Change
        case 0:
        {
            if ([self.olHopping isOn])
            {
                dispatch_async(dispatch_get_main_queue(),
                               ^{
                                   [imageAlert show:YES target:self];
                               });
                [self performSelector:@selector(startHopping) withObject:nil afterDelay:2.5f];
                
            }
            else
            {
                [Indicator show:YES target:self];
                [self.m_RFIDDevice SetFrequencyHoppingMode:0x00];
            }
        }
            break;
            
            
            
            //Error Hoping Mode
        case 1:
        {
            [Indicator show:NO target:self];
            [self.m_RFIDDevice GetFrequencyHoppingMode];
            
        }
            break;
            
        default:
            break;
    }
}

- (void) startHopping
{
    [imageAlert show:NO target:self];
    [Indicator show:YES target:self];
    
    /* setOptimumFrequencyHoppingTable + Smart hopping On  */
    [self.m_RFIDDevice setOptimumFrequencyHoppingTable];
}


- (void)didSetOptiFreqHPTable:(uint8_t)status
{
    if(status == 0x00)
    {
        //Testing
    }
    
    if(status == 0x01)
    {
        dispatch_async(dispatch_get_main_queue(),^{
        [imageAlert show:NO target:self];
        [Indicator show:NO target:self];
        
      
            
        UIAlertView *selectHPMode = [[UIAlertView alloc] initWithTitle:@"update"
                                                               message:@"Compleate updated"
                                                              delegate:nil
                                                     cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                     otherButtonTitles:nil];
        [selectHPMode setTag:100];
        [selectHPMode show];
        
        });
    }
    
  
    
}

- (void)didSetFreqHPTable:(uint8_t)status
{
    
    if(status == 0x00)
    {
        dispatch_async(dispatch_get_main_queue(),^{
            [imageAlert show:NO target:self];
            [Indicator show:NO target:self];
            
            
            
            UIAlertView *selectHPMode = [[UIAlertView alloc] initWithTitle:@"update"
                                                                   message:@"compleate updated "
                                                                  delegate:nil
                                                         cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                         otherButtonTitles:nil];
            [selectHPMode setTag:100];
            [selectHPMode show];
            
        });
    }

    
}



- (IBAction)actionHoppingOnOff:(id)sender
{
    
    NSString* alertString = @"";
    if ([self.olHopping isOn])
    {
        alertString = @"This process will choose optimum channel for operation.\nFor proper poeration, don't block antenna during process.\nIt takes up to 10 seconds";
        dispatch_async(dispatch_get_main_queue(),^{
            UIAlertView *selectHPMode = [[UIAlertView alloc] initWithTitle:@"Hopping"
                                                                   message:alertString
                                                                  delegate:self
                                                         cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                         otherButtonTitles:nil];
            [selectHPMode setTag:0];
            [selectHPMode show];
            
        });
    }
    else
    {
        alertString = @"Normal mode feature disables smart hopping feature";
        [self.m_RFIDDevice setSmartHoppingOnOff:NO];
        dispatch_async(dispatch_get_main_queue(),^{
            UIAlertView *selectHPMode = [[UIAlertView alloc] initWithTitle:@"Hopping"
                                                                   message:alertString
                                                                  delegate:nil
                                                         cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                         otherButtonTitles:nil];
            [selectHPMode setTag:100];
            [selectHPMode show];
            
        });
    }

}



/***********************************************************************/
#pragma mark Hopping for Dongle and Combo
/***********************************************************************/

- (void)freqHPTableReceived:(uint8_t)status
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       switch (status) {
                           case 0x00:
                               //Normal mode
                               NSLog(@"HOPPING: NO");
                               [self.olHopping setOn:NO];
                               break;
                               
                           case 0x01:
                               // Smart Hopping mode
                               NSLog(@"HOPPING: YES");
                               [self.olHopping setOn:YES];
                               break;
                           default:
                               break;
                       }
                       
                   });
}


- (void)readerInfoReceived:(NSData *)data
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        CommonReaderInfo *info = [CommonReaderInfo sharedInstance];
        self.olOnOffTime.text     =  [NSString stringWithFormat:@"%d/%d",info.nRFIDonTime,info.nRFIDoffTime];
    });
}



- (void)errReceived:(NSData *)errCode{
    Byte *b = (Byte*) [errCode bytes];
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       switch (b[1])
                       {
                           case 0xE4:
                           case 0xE6:
                           case 0xD2:
                           {
                               
                            
                               UIAlertView *errorHopping
                               = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"The retrun loss of antenna is too large to optimize channel."
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"OK", @"")
                                                  otherButtonTitles:nil];
                               [errorHopping setTag:1];
                               [errorHopping show];
 
                               break;
                           }
                           default:
                               
                               break;
                       }
                   });
}


#pragma mark - RFID Delegate
- (void)channelReceived:(uint8_t)channel channelOffset:(uint8_t)channelOffset
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
         CommonReaderInfo *info = [CommonReaderInfo sharedInstance];
        self.m_Model.m_RFIDDeviceInfo.m_nRFID_RF_CHANNEL = info.nRFIDchannel;
        [self.olRFChannel setText:[NSString stringWithFormat:@"%d",info.nRFIDchannel]];
    });
}


- (void)txPowerLevelReceived:(NSData*)power
{
    CommonReaderInfo *info = [CommonReaderInfo sharedInstance];
    dispatch_async(dispatch_get_main_queue(),
   ^{
       self.olPowerLever.text     = [NSString stringWithFormat:@"%02.1f", info.fRFIDpower/10.0];}
    );

}




/***********************************************************************/
#pragma mark AntiCollisionMode
/***********************************************************************/
- (void)anticolParamReceived:(uint8_t)mode Counter:(uint8_t)counter
{
    ModelSettings *model = [ModelSettings sharedInstance];
    model.m_RFIDDeviceInfo.m_nAntiMode   = mode;
    dispatch_async(dispatch_get_main_queue(),
    ^{
      self.olAntiCollisionMode.text = [NSString stringWithFormat:@"Mode :%d", model.m_RFIDDeviceInfo.m_nAntiMode];
    });
    
}

- (void)didSetAntiCol:(uint8_t)status
{
    switch (status) {
        case 0x00:
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Anti collision Updated." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
            });
        }
            break;
        default:
            break;
    }
}

@end

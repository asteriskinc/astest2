//
//  Indicator.h
//  hitejinro
#import <UIKit/UIKit.h>

@interface Indicator : UIView
+(void)show:(BOOL)on target:(UIViewController*)viewController;
@end

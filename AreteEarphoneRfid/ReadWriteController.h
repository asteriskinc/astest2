//  ReadWriteController.h

#import <UIKit/UIKit.h>
#import "protocols.h"


@interface ReadWriteController : UIViewController <UITextFieldDelegate, RcpCommonDelegate,RcpRFIDDelegate>{
   // NSData *pcData;
}

//@property (nonatomic, strong, readwrite) NSData *epc;
@property (weak, nonatomic) IBOutlet UISegmentedControl *olMode;
- (IBAction)eModeChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *olAccessPassword;
@property (weak, nonatomic) IBOutlet UITextField *olStartAdress;
@property (weak, nonatomic) IBOutlet UITextField *olLength;
@property (weak, nonatomic) IBOutlet UITextField *olData;
@property (weak, nonatomic) IBOutlet UISegmentedControl *olTargetMemory;
@property (weak, nonatomic) IBOutlet UITextField *olTargetEpc;
- (IBAction)eTargetMemoryChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *olDataTitle;

- (IBAction)actLimitEPCLength:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *olScrollVeiw;
- (IBAction)rightBarButtonItemClicked:(id)sender;
@end

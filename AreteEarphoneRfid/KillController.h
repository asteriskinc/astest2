//  KillController.h

#import <UIKit/UIKit.h>
#import "protocols.h"

@interface KillController : UIViewController <RcpRFIDDelegate>
@property (weak, nonatomic) IBOutlet UITextField *olTargetEpc;
@property (weak, nonatomic) IBOutlet UITextField *olKillPassword;
- (IBAction)rightBarButtonItemClicked:(id)sender;
@end

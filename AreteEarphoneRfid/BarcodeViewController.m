//  BarcodeViewController.m
//  Created by Robin on 2016. 8. 18..


#import "BarcodeViewController.h"
#import "ModelSettings.h"
#import "GlobalDefine.h"
@interface BarcodeViewController ()

@property (nonatomic,retain) ModelSettings *m_Model;
@property (weak, nonatomic) IBOutlet UITextView *olBarcodeDetailData;
@end

@implementation BarcodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.m_Model          = [ModelSettings sharedInstance];
    NSDictionary    *dic  = [self.m_Model.m_arReadTagData objectAtIndex:self.m_Model.m_nSelectTagNum];
     NSString *strData    = [dic objectForKey:df_CELL_TAG_INFO];
    [self.olBarcodeDetailData setText:strData];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

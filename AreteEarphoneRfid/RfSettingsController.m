//  RfSettingsController.m

#import "RfSettingsController.h"
#import "ModelSettings.h"

/*SDK header files*/
#import "ComboDevices.h"

@interface RfSettingsController ()


@property (nonatomic,retain)ModelSettings      *m_Model;
@property (nonatomic,retain)ComboRFIDApi       *m_RFIDDevice;
@end

@implementation RfSettingsController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.m_Model         = [ModelSettings sharedInstance];
    [self.olChannel setText:@"--"];
    [self.olChannel setText:[NSString stringWithFormat:@"%d",
     self.m_Model.m_RFIDDeviceInfo.m_nRFID_RF_CHANNEL]];
    
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.m_RFIDDevice    = [ComboRFIDApi sharedInstance];
    CommonDevice *device = [CommonDevice sharedInstance];
    device.delegateRFID = self;
}

- (IBAction)rightBarButtonItemClicked:(id)sender
{    
    [self.olChannel resignFirstResponder];
    [self.m_RFIDDevice setChannel:[self.olChannel.text integerValue] channelOffset:0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [self setOlChannel:nil];
    [super viewDidUnload];
}

- (void)didSetChParamReceived:(uint8_t)statusCode
{
    if(statusCode == 0x00)
    {
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:nil message:@"Changed Channel Data" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                     [alert show];
                   });
    }

    
}

- (void)channelReceived:(uint8_t)channel channelOffset:(uint8_t)channelOffset
{
    dispatch_async(dispatch_get_main_queue(),
    ^{
        [self.olChannel setText:[NSString stringWithFormat:@"%d",channel]];
    });
}

@end

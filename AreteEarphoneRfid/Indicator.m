//
//  Indicator.m
//  hitejinro
//
#import "Indicator.h"

@implementation Indicator

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


+(void)show:(BOOL)on target:(UIViewController*)viewController
{
    static dispatch_once_t pred = 0;
    __strong static UIActivityIndicatorView *indicator = nil;
    __strong static UILabel *loadingLabel = nil;
    
    dispatch_once(&pred,
                  ^{

                      indicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
                      loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 50)];
                      
                  });
    
    if(indicator != nil)
    {
        indicator.frame = viewController.view.frame;
        indicator.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
        indicator.clipsToBounds = YES;
        //indicator.layer.cornerRadius = 10.0;
        indicator.hidesWhenStopped = YES;
        
        [indicator setCenter:CGPointMake(viewController.view.frame.size.width/2, viewController.view.frame.size.height/2)];
        

        if(on == YES)
        {
            [viewController.view addSubview:indicator];
            [indicator startAnimating];
            
            if(loadingLabel !=nil)
            {
                [loadingLabel setCenter:CGPointMake(indicator.center.x, indicator.center.y + 50)];
                loadingLabel.textColor = [UIColor whiteColor];
                loadingLabel.textAlignment = NSTextAlignmentCenter;
                [loadingLabel setFont:[UIFont systemFontOfSize:20]];
                loadingLabel.text = @"Please Wait";
            }
            loadingLabel.hidden = NO;
            [viewController.view addSubview:loadingLabel];            
        }
        else
        {
            [indicator stopAnimating];
            [indicator removeFromSuperview];
            
            loadingLabel.hidden = YES;
            [loadingLabel removeFromSuperview];
        }
    }
}

@end

//  TagAccessController.m

#import "TagAccessController.h"
#import "PhyUtility.h"
#import "ModelSettings.h"
#import "GlobalDefine.h"



/*SDK header files*/
#import "ComboDevices.h"
#import "typeDefine.h"


@implementation TagAccessController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ModelSettings *model            = [ModelSettings sharedInstance];
    NSDictionary    *dic            = [model.m_arReadTagData objectAtIndex:model.m_nSelectTagNum];
    
    NSString *strDeviceType =  @"";
    int nDeviceType         = [[dic objectForKey:df_CELL_TAG_DEVICETYPE]intValue];
    
    
    switch (nDeviceType)
    {
            
        case RCP_COMBO_DEVICE_RFID:
            strDeviceType             = @"RFID";
            break;
            
        case RCP_COMBO_DEVICE_BARCODE:
            strDeviceType             = @"BARCODE";
            break;
            
        case RCP_COMBO_DEVICE_NFC:
             strDeviceType            = @"NFC";
            break;
            
        default:
            strDeviceType             = @"UNKNOW";
            break;
    }
    NSData *rawData  = [dic objectForKey:df_CELL_TAG_RAW];
    NSString *strRaw = [rawData bytesToHexString];
    NSString *strData = [NSString stringWithFormat:@"DevideType : %@ \nData : %@  \nRAW DATA : %@ \nRead Count %@"
                         ,strDeviceType
                         ,[dic objectForKey:df_CELL_TAG_INFO]
                         ,strRaw
                         ,[dic objectForKey:df_CELL_TAG_COUNT]
                         ];
    
    
    self.olDataView.text = strData;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end

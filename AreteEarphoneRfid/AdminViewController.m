//  AdminViewController.m

#import "AdminViewController.h"

/*SDK header files*/
#import "ComboDevices.h"


@interface AdminViewController ()
@property (nonatomic, retain) CommonDevice  *m_DeviceCommon;
@property (weak, nonatomic) IBOutlet UITextField *olPassword;
@property (weak, nonatomic) IBOutlet UISegmentedControl *olSegment;
@property (weak, nonatomic) IBOutlet UIButton *olSetMode;
@property (weak, nonatomic) IBOutlet UILabel *olLabelProgMode;
- (IBAction)actionPw:(id)sender;
- (IBAction)actionMode:(id)sender;
@end

@implementation AdminViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //self.rcp.delegateRFID = self;
    self.m_DeviceCommon  = [CommonDevice sharedInstance];
    self.m_DeviceCommon.delegateCommon = self;
    self.m_DeviceCommon.delegateRFID   = self;
    
    self.olSegment.hidden = YES;
    self.olSetMode.hidden = YES;
    self.olLabelProgMode.hidden = YES;
    
}


- (IBAction)actionPw:(id)sender
{
    
    if([self.olPassword.text isEqualToString:@"111111"])
    {
        self.olSegment.hidden = NO;
        self.olSetMode.hidden = NO;
        self.olLabelProgMode.hidden = NO;
    }
    [self.olPassword setText:@""];
    [self.view endEditing:YES];
}

- (IBAction)actionMode:(id)sender
{
    uint8_t mode = (uint8_t) self.olSegment.selectedSegmentIndex;
    //printf("mode = %d\n", mode);
    [self.m_DeviceCommon setReaderProgMode:mode];
}

-(void)responseReboot:(uint8_t)status{
    // after reboot
    
    switch (status) {
        case 0x00:

            
            break;
        default:
            break;
    }
}

-(BOOL)checkDeviceBattery{
    float deviceBattery = [UIDevice currentDevice].batteryLevel;
    if((deviceBattery*100) >  50){
        return YES;
    }else{
        return NO;
    }
}


@end

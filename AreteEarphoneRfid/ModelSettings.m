//  ModelSettings.m
//  Created by Robin on 2016. 2. 22..

#import "ModelSettings.h"
#import "GlobalDefine.h"


@implementation RFIDDeviceInfo
-(id)init
{
    self = [super init];
    if(self)
    {
        self.m_fRFIDPower           = 0.0;
        self.m_nRFID_RF_CHANNEL     = 0;
    }
    return self;
}
@end


@implementation ModelSettings

+ (id)  sharedInstance
{
    static dispatch_once_t dispatchOnce;
    static ModelSettings   *instance    = nil;
    
    dispatch_once(&dispatchOnce, ^{
        instance                  = [[self alloc] init];
        instance.m_arReadTagData  = [[NSMutableArray alloc]init];
        instance.m_RFIDDeviceInfo = [[RFIDDeviceInfo alloc]init];
    });
    
    return instance;
}



@end

//  ReadWriteController.m

#import "ReadWriteController.h"
#import "PhyUtility.h"
#import "ModelSettings.h"
#import "GlobalDefine.h"

/*SDK header files*/
#import "ComboDevices.h"
@interface ReadWriteController ()


@property (nonatomic,retain)ModelSettings *m_Model;
@property (nonatomic,retain)ComboRFIDApi  *m_RFIDDevice;
@end

@implementation ReadWriteController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.olScrollVeiw.scrollEnabled = YES;
    self.olScrollVeiw.contentSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height + 200);
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:self.view.window];
    
    
    
}

-(void) setViewMoveUp:(BOOL)moveUp
{
    if(moveUp)
    {
        NSLog(@"Scroll MOVE UP");
        [UIView animateWithDuration:0.2 animations:^{self.olScrollVeiw.contentOffset = CGPointMake(0.0f, 100.f);}];
    }
    else
    {
        NSLog(@"Scroll MOVE Down");
        [UIView animateWithDuration:0.2 animations:^{self.olScrollVeiw.contentOffset = CGPointMake(0.0f, self.olScrollVeiw.frame.origin.y-80);}];
    }
}



- (void)keyboardWillShow:(NSNotification *)notif
{
    
    //olData
    if([self.olData isFirstResponder] &&self.view.frame.origin.y >=0)
    {
        [self setViewMoveUp:YES];
    }
    else if ([self.olData isFirstResponder]&& self.view.frame.origin.y <0)
    {
        [self setViewMoveUp:NO];
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)txtField
{
    [self setViewMoveUp:NO];
    [txtField resignFirstResponder];
    NSLog(@"Resturn");
    return NO;
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.m_RFIDDevice    = [ComboRFIDApi sharedInstance];
    self.m_Model         = [ModelSettings sharedInstance];
    
    
    self.olAccessPassword.delegate  = self;
    self.olStartAdress.delegate     = self;
    self.olLength.delegate          = self;
    self.olData.delegate            = self;
    self.olTargetEpc.delegate       = self;
    
    
    NSDictionary    *dic  = [self.m_Model.m_arReadTagData objectAtIndex:self.m_Model.m_nSelectTagNum];
    self.olTargetEpc.text = [PhyUtility getEpcStringFromPcEPCData:[dic objectForKey:df_CELL_TAG_RAW]];
    self.m_RFIDDevice.delegateCommon = self;
    self.m_RFIDDevice.delegateRFID   = self;
    
}

- (void)tagMemoryReceived:(NSData *)data
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       NSString *strData;
                       if (self.olTargetMemory.selectedSegmentIndex == 1)
                       {
                           strData = [self dataToEpcInteger:data];
                       }
                       else
                       {
                           strData = [data bytesToHexString];
                       }
                       self.olData.text = strData;
                   });
}
- (NSString *) dataToEpcInteger:(NSData *)data
{
    uint8_t* pData = (uint8_t*)[data bytes];
    uint16_t intData = pData[0] << 8 | pData[1];
    intData = intData >>11;
    return [NSString stringWithFormat:@"%d",intData];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)eModeChanged:(id)sender
{
    if(self.olMode.selectedSegmentIndex == 0)
    {
        self.navigationItem.title = @"Read";
    }
    else
    {
        self.navigationItem.title = @"Write";
    }
}

- (IBAction)rightBarButtonItemClicked:(id)sender
{
    [self.olTargetEpc resignFirstResponder];
    [self.olAccessPassword resignFirstResponder];
    [self.olStartAdress resignFirstResponder];
    [self.olLength resignFirstResponder];
    [self.olData resignFirstResponder];
    [self setViewMoveUp:NO];
    NSString  *hexaString = self.olAccessPassword.text;
    NSScanner *pScanner = [NSScanner scannerWithString: hexaString];
    uint32_t  accesspassword;
    [pScanner scanHexInt: &accesspassword];
    
    uint16_t  startAddress = [self.olStartAdress.text intValue];
    uint16_t  dataLength = [self.olLength.text intValue];
    uint8_t   membank;
    
    
    switch(self.olTargetMemory.selectedSegmentIndex)
    {
        case 0:
            membank = 0;
            break;
        case 1: //PC
        case 2: //EPC
            membank = 1;
            break;
        case 3: // TID
            membank = 2;
            break;
        case 4:
            membank = 3;
            break;
    }
    
    NSData *data      = [self.olData.text hexStringToBytes];
    NSData *epcs      = [self.olTargetEpc.text hexStringToBytes];
    NSData *pcData    = [epcs subdataWithRange:NSMakeRange(0, 2)];
    
    if(epcs.length == 0)
        return;
    
    if(self.olMode.selectedSegmentIndex == 0)
    {
       
        
            [self.m_RFIDDevice readFromTagMemory:accesspassword
                                             epc:epcs
                                      memoryBank:membank
                                    startAddress:startAddress
                                      dataLength:dataLength];
        
    }
    else
    {
        if (self.olTargetMemory.selectedSegmentIndex == 1)
        {
            uint8_t* pData = (uint8_t*)[pcData bytes];
            uint16_t intData = pData[0] << 8 | pData[1];
            int epcLength = [self.olData.text intValue];
            if (epcLength != 0) {
                intData = intData & 0x07FF;
                intData |= (epcLength& 0x1F) << 11;
            }
            data = [[NSString stringWithFormat:@"%04X",intData]hexStringToBytes];
        }
        [self.m_RFIDDevice writeToTagMemory:accesspassword
                                        epc:epcs
                                 memoryBank:membank
                               startAddress:startAddress
                                dataToWrite:data];
    }
}


- (void)writedReceived:(uint8_t)statusCode
{
    
    if(statusCode == 0x00)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm"
                                                        message:@"Done Write"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           [alert show];
                       });
    }
    
}


- (void)errReceived:(NSData *)errCode
{
    uint8_t *pData = (uint8_t*)[errCode bytes];
    
    NSString *strErrCode = @"";
    NSString *strSubCode = @"";
    switch (pData[0])
    {
        case 0x09 : strErrCode =@"Failure to read the tag memory"; break;
        case 0x10 : strErrCode =@"Failure to write data"; break;
        case 0x0B : strErrCode =@"Read Type C Tag ID Multiple’ in Operation"; break;
        case 0x0D : strErrCode =@"Not in mode ‘Read Type C Tag ID Multiple’"; break;
        case 0x0E : strErrCode =@"Invalid parameter"; break;
        case 0x12 : strErrCode =@"Failure to kill a tag"; break;
        case 0x13 : strErrCode =@"Failure to lock a tag"; break;
        case 0x15 : strErrCode =@"Failure to read a tag"; break;
        case 0x18 : strErrCode =@"Not supported command"; break;
        case 0xFF : strErrCode =@"CRC Error"; break;
        default:
        {
            NSString *strTem = [NSString stringWithFormat:@"0x%x",pData[1]];
            strErrCode =strTem;
        }
            break;
    }
    
    
    switch (pData[1])
    {
            
        case  0x01 :strSubCode = @" Not supported "; break;
        case  0x02 :strSubCode = @" Insufficient privileges"; break;
        case  0x03 :strSubCode = @" Memory overrun"; break;
        case  0x04 :strSubCode = @" Memory locked"; break;
        case  0x05 :strSubCode = @" Crypto suite error"; break;
        case  0x06 :strSubCode = @" Command not encapsulated"; break;
        case  0x07 :strSubCode = @" ResponseBuffer overflow"; break;
        case  0x08 :strSubCode = @" Security timeout"; break;
        case  0x0B :strSubCode = @" Insufficient power"; break;
        case  0x0F :strSubCode = @" Non- specific error"; break;
        case  0x11 :strSubCode = @" Sensor Scheduling configuration"; break;
        case  0x12 :strSubCode = @" Tag Busy"; break;
        case  0x13 :strSubCode = @" Measurement type not supported"; break;
        case  0x80 :strSubCode = @" No tag detected"; break;
        case  0x81 :strSubCode = @" Handle acquisition failure"; break;
        case  0x82 :strSubCode = @" Access password failure"; break;
        case  0x90 :strSubCode = @" CRC error"; break;
        case  0x91 :strSubCode = @" Rx Timeout"; break;
        case  0xA0 :strSubCode = @" Registry update failure"; break;
        case  0xA1 :strSubCode = @" Registry erase failure"; break;
        case  0xA2 :strSubCode = @" Registry write failure"; break;
        case  0xA3 :strSubCode = @" Registry not exist"; break;
        case  0xB0 :strSubCode = @" UART failure"; break;
        case  0xB1 :strSubCode = @" SPI failure"; break;
        case  0xB2 :strSubCode = @" I2C failure"; break;
        case  0xB3 :strSubCode = @" GPIO failure"; break;
        case  0xE0 :strSubCode = @" Not supported command"; break;
        case  0xE1 :strSubCode = @" Undefined command"; break;
        case  0xE2 :strSubCode = @" Invalid parameter"; break;
        case  0xE3 :strSubCode = @" Too high parameter"; break;
        case  0xE4 :strSubCode = @" Too low parameter"; break;
        case  0xE5 :strSubCode = @" Failure automatic read operation"; break;
        case  0xE6 :strSubCode = @" Not automatic read mode"; break;
        case  0xE7 :strSubCode = @" Failure to get last response"; break;
        case  0xE8 :strSubCode = @" Failure to control test"; break;
        case  0xE9 :strSubCode = @" Failure to reset Reader"; break;
        case  0xEA :strSubCode = @" Rfidblock control failure"; break;
        case  0xEB :strSubCode = @" Automatic read in operation"; break;
        case  0xF0 :strSubCode = @" Undefined other error"; break;
        case  0xF1 :strSubCode = @" Failure to verify write operation"; break;
        case  0xFC :strSubCode = @" Abnormal antenna"; break;
        case  0xFF :strSubCode = @" None error"; break;
            
        default:
        {
            NSString *strTem = [NSString stringWithFormat:@"0x%x",pData[1]];
            strSubCode =strTem;
        }
            break;
    }
    
    NSString *strErrTotal = [NSString stringWithFormat:@"Error Code : %@ \n SubError: %@",strErrCode,strSubCode];
    
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:strErrTotal
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       [alert show];
                   });
}

- (void)viewDidUnload {
    [self setOlAccessPassword:nil];
    [self setOlMode:nil];
    [self setOlStartAdress:nil];
    [self setOlLength:nil];
    [self setOlTargetMemory:nil];
    [self setOlData:nil];
    [self setOlTargetEpc:nil];
    [self setOlScrollVeiw:nil];
    [super viewDidUnload];
}





- (IBAction)eTargetMemoryChanged:(id)sender {
    
    self.olDataTitle.text = @"Data (HEX)";
    [self.olData resignFirstResponder];
    [self.olData setKeyboardType:UIKeyboardTypeDefault];
    if (self.olTargetMemory.selectedSegmentIndex == 1)
    {
        //PC
        self.olStartAdress.text     = @"1";
        self.olLength.text          = @"1";
        self.olStartAdress.enabled  = NO;
        self.olLength.enabled       = NO;
        self.olDataTitle.text       = @"EPC Length";
        [self.olData setKeyboardType:UIKeyboardTypeNumberPad];
    }
    else if(self.olTargetMemory.selectedSegmentIndex == 2)
    {
        //EPC
        self.olStartAdress.text     = @"2";
        self.olLength.text          = @"0";
        self.olStartAdress.enabled  = NO;
        self.olLength.enabled       = NO;
    }
    else
    {
        self.olStartAdress.enabled  = YES;
        self.olLength.enabled       = YES;
    }
}

- (IBAction)actLimitEPCLength:(id)sender {
    if (self.olTargetMemory.selectedSegmentIndex == 1)
    {

        if ([self.olData.text intValue] > 31) {
            self.olData.text = @"31";
        }
    }
}
@end




//
//  NSLoggerSupport.m
//  AsKey
//
//  Created by niwatako on 2015/10/05.
//  Copyright (c) 2015年 asterisk. All rights reserved.
//

#import "Common.h"
#import "NSLoggerSupport.h"

/* ==================== */
#pragma mark - デバッグビルド時のNSLoggerへのログ出力
/* -------------------- */
#ifdef DEBUG

/** NSLogger
 NSLogger Client をインストールし、
 Preferences より Bonjor Service Name を NSLoggerBuildUserName に設定して下さい。
 */
#import "NSLogger.h"
#endif



@implementation NSLoggerSupport

+ (void)startLogger {
#ifdef DEBUG
    LoggerStartForBuildUser();
    NSLog(@"startNSLogger");
#endif
}

@end

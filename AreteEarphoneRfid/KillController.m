//  KillController.m

#import "KillController.h"
#import "PhyUtility.h"
#import "ModelSettings.h"
#import "GlobalDefine.h"


/*SDK header files*/
#import "ComboDevices.h"


@interface KillController ()
@property (nonatomic,retain)ComboRFIDApi *m_RFIDDevice;
@end

@implementation KillController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.m_RFIDDevice    = [ComboRFIDApi sharedInstance];
    self.m_RFIDDevice.delegateRFID = self;
    
    ModelSettings *model = [ModelSettings sharedInstance];
    NSDictionary    *dic  = [model.m_arReadTagData objectAtIndex:model.m_nSelectTagNum];
    self.olTargetEpc.text = [PhyUtility getEpcStringFromPcEPCData:[dic objectForKey:df_CELL_TAG_RAW]];
    
}

- (IBAction)rightBarButtonItemClicked:(id)sender
{
    [self.olTargetEpc resignFirstResponder];
    
    NSString *hexaString = self.olKillPassword.text;
    NSScanner* pScanner = [NSScanner scannerWithString: hexaString];
    uint32_t killpassword;
    [pScanner scanHexInt: &killpassword];
    NSData *epcs       = [self.olTargetEpc.text hexStringToBytes];
    [self.m_RFIDDevice killTag:killpassword epc:epcs];
}

- (void)killedReceived:(uint8_t)statusCode
{
    if(statusCode == 0x00)
    {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm"
                                                    message:@"Killed Tag"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       [alert show];
                   });
    }

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)failureReceived:(NSData*)errCode
{
    NSString *strErrMsg = [NSString stringWithFormat:@"Error Code: 0x%02X",
                           ((const uint8_t *)errCode.bytes)[0]];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:strErrMsg
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       [alert show];
                   });
}

- (void)viewDidUnload {
    [self setOlTargetEpc:nil];
    [self setOlKillPassword:nil];
    [super viewDidUnload];
}
@end

//  AboutController.m
//
#import "AboutController.h"
#import "ModelSettings.h"
#import "GlobalDefine.h"

/*SDK header files*/
#import "ComboDevices.h"
@interface AboutController()
@property(nonatomic, retain)  ModelSettings *m_Model;
@property(nonatomic, retain)  CommonDevice *m_CommonDevice;
@property (weak, nonatomic) IBOutlet UILabel *olAppVersion;
@property (weak, nonatomic) IBOutlet UILabel *olModel;
@property (weak, nonatomic) IBOutlet UILabel *olRegion;
@property (weak, nonatomic) IBOutlet UILabel *olBattery;
@property (weak, nonatomic) IBOutlet UILabel *olHWV;
@property (weak, nonatomic) IBOutlet UILabel *olFWV;
@property (weak, nonatomic) IBOutlet UILabel *olRFODModule;
@property (weak, nonatomic) IBOutlet UILabel *olAppInfo;
@end

@implementation AboutController
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.m_Model           = [ModelSettings sharedInstance];
    self.olAppVersion.text = [SDeviceApi getSDKVersion];
    self.m_CommonDevice    = [CommonDevice sharedInstance];
    self.m_CommonDevice.delegateCommon = self;
    self.m_CommonDevice.delegateRFID   = self;
 
    [self updateControl];
    [self.olAppInfo setText:[self appNameAndVersionNumberDisplayString]];
    
    ComboRFIDApi *device = [ComboRFIDApi sharedInstance];
    [device getRegion];
    [self performSelector:@selector(onTick) withObject:nil afterDelay:0.5f];
    
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
}


-(void) updateControl
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       
                       CommonReaderInfo  *info = [CommonReaderInfo sharedInstance];
                       self.olHWV.text         = info.strhardware;
                       self.olFWV.text         = info.strfirmware;
                       self.olModel.text       = info.strmodelNumber;
                       
                       
                   });
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source


- (void) onTick
{
    //Get RFID Version Info
    ComboRFIDApi *device = [ComboRFIDApi sharedInstance];
    [device getRFIDMoudleVersion];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segueAdmin"])
    {

    }
}


- (void)adcReceived:(NSData*)data
{
    Byte *b    = (Byte*) [data bytes];
    int adc    = b[0];
    dispatch_async(dispatch_get_main_queue(),
    ^{
        self.olBattery.text    = [NSString stringWithFormat:@"%d %%",adc];
        
    });
}


- (void)viewDidUnload
{
    [self setOlAppVersion:nil];
    [self setOlModel:nil];
    [self setOlHWV:nil];
    [self setOlRegion:nil];
    [self setOlBattery:nil];
    [self setOlFWV:nil];
    [super viewDidUnload];
}


/* Region Infomation */
- (void)regionReceived:(uint8_t)region
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       CommonReaderInfo  *info = [CommonReaderInfo sharedInstance];
                       self.olRegion.text = info.strRegion;
                   });
}


/* RFID Module Version Infomation */
- (void)rfidModuleVersionReceived
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       CommonReaderInfo  *info = [CommonReaderInfo sharedInstance];
                       self.olRFODModule.text = info.strRFIDModuleVersion;
                   });

}

/* APP version */
- (NSString *)appNameAndVersionNumberDisplayString
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];
    return [NSString stringWithFormat:@"%major :%@ (%@)", majorVersion, minorVersion];
}

@end

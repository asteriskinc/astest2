//  LockController.m

#import "LockController.h"
#import "PhyUtility.h"
#import "ModelSettings.h"
#import "GlobalDefine.h"


/*SDK header files*/
#import "ComboDevices.h"


@interface LockController ()
@property (nonatomic,retain)ComboRFIDApi *m_RFIDDevice;

@end

@implementation LockController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.m_RFIDDevice    = [ComboRFIDApi sharedInstance];


    CommonDevice *device = [CommonDevice sharedInstance];
    device.delegateRFID = self;
    
    ModelSettings *model = [ModelSettings sharedInstance];
    NSDictionary    *dic  = [model.m_arReadTagData objectAtIndex:model.m_nSelectTagNum];
    self.olTargetEpc.text = [PhyUtility getEpcStringFromPcEPCData:[dic objectForKey:df_CELL_TAG_RAW]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)lockedReceived:(uint8_t)statusCode
{
    if(statusCode == 0x00)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Confirm"
                                                        message:@"Lock Tag"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           [alert show];
                       });
    }
}


- (IBAction)rightBarButtonItemClicked:(id)sender
{
    [self.olTargetEpc resignFirstResponder];
    
    NSString *hexaString = self.olAccessPassword.text;
    NSScanner* pScanner = [NSScanner scannerWithString: hexaString];
    uint32_t accesspassword;
    [pScanner scanHexInt: &accesspassword];
    
    uint32_t lock = 0;
    uint32_t seed = (uint32_t)self.olAction.selectedSegmentIndex;
        
    switch (self.olTargetMemory.selectedSegmentIndex)
    {
        case 0: // Kill
            lock = (seed << 8) | (3 << 18);
            break;
        case 1: // Access
            lock = (seed << 6) | (3 << 16);
            break;
        case 2: // EPC
            lock = (seed << 4) | (3 << 14);
            break;  
        case 3: // TID
            lock = (seed << 2) | (3 << 12);
            break;
        case 4: // USER
            lock = (seed << 0) | (3 << 10);
            break;
        default:
            break;
    }

     NSData *epcs       = [self.olTargetEpc.text hexStringToBytes];
    [self.m_RFIDDevice lockTagMemory:accesspassword
                        epc:epcs
                   lockData:lock];

}

- (void)failureReceived:(NSData*)errCode
{
    NSString *strErrMsg = [NSString stringWithFormat:@"Error Code: 0x%02X",
                           ((const uint8_t *)errCode.bytes)[0]];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                    message:strErrMsg
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    
    dispatch_async(dispatch_get_main_queue(),
    ^{
            [alert show];
    });
}

- (void)viewDidUnload {
    [self setOlTargetEpc:nil];
    [self setOlAccessPassword:nil];
    [self setOlTargetMemory:nil];
    [self setOlAction:nil];
    [super viewDidUnload];
}
@end

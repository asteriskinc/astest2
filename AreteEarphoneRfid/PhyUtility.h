//
//  PhyUtility.h

#import <Foundation/Foundation.h>

@interface PhyUtility : NSObject

+(NSString*)getEpcStringFromPcEPCData:(NSData*)pcEpcData;
@end

@interface NSString (NSStringHexToBytes)
- (NSData*) hexStringToBytes;
@end
@interface NSString (NSStringToHex)
- (NSData*) stringToHex;
@end

@interface NSData (NSDataToHexString)
- (NSString*) bytesToHexString;
@end




@interface UIApplication (Logging)
+ (void) redirectConsoleLogToDocumentFolder:(NSString*)filename;
@end
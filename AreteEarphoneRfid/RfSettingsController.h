//  SettingsFhLbtController.h

#import <UIKit/UIKit.h>
#import "protocols.h"
@interface RfSettingsController : UIViewController  <RcpRFIDDelegate>
@property (weak, nonatomic) IBOutlet UITextField *olChannel;
- (IBAction)rightBarButtonItemClicked:(id)sender;
@end

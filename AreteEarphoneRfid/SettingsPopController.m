//  SettingPopController.m

#import "SettingsPopController.h"
#import "ModelSettings.h"

/*SDK header files*/
#import "ComboDevices.h"

@interface SettingsPopController ()
{
    BOOL mIsBeep;
    BOOL mIsVib;
    BOOL mIsLed;
    BOOL mIsIllu;
}
- (IBAction)eBeepChanged:(id)sender;
- (IBAction)eVibrationChanged:(id)sender;
- (IBAction)eLEDChanged:(id)sender;
- (IBAction)eIlluminationChanged:(id)sender;
- (IBAction)eAutoPower:(id)sender;
- (IBAction)eTriggerDefault:(id)sender;
- (IBAction)eFactoryReset:(id)sender;

@property(nonatomic,retain) CommonDevice  *m_Commdevice;
@property(nonatomic,retain) ModelSettings *m_Model;
@property (weak, nonatomic) IBOutlet UISwitch *olBeep;
@property (weak, nonatomic) IBOutlet UISwitch *olIllumination;
@property (weak, nonatomic) IBOutlet UISwitch *olVibration;
@property (weak, nonatomic) IBOutlet UISwitch *olLED;
@property (weak, nonatomic) IBOutlet UISwitch *olAutoPower;
@property (weak, nonatomic) IBOutlet UISwitch *olDefaultTriggerOn;
@property  (weak,nonatomic) IBOutlet UITableViewCell *olCellFactory;
@property (nonatomic,retain) UIAlertView*  mAlert;
//@property (weak, nonatomic) IBOutlet UILabel *olLabeFactoryStatus;
@end

@implementation SettingsPopController


- (IBAction)eFactoryReset:(id)sender
{
     [self showAlert:@"Request Reset!!"];
    ComboBarcodeApi *barcode = [ComboBarcodeApi sharedInstance];
    [barcode setFactoryReset];
    
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.m_Commdevice = [CommonDevice sharedInstance];
    self.m_Model      = [ModelSettings sharedInstance];
    self.m_Commdevice.delegateCommon  = self;
    self.mAlert = [[UIAlertView alloc]initWithTitle:nil message:@"" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    
    
    
    BOOL isAutoPower  = [[NSUserDefaults standardUserDefaults] boolForKey:@"AutoPowerOn"];
    BOOL isDefaultTriggerOn  = [[NSUserDefaults standardUserDefaults] boolForKey:@"DefaultTriggerOn"];
    BOOL isBeepOn  =[[NSUserDefaults standardUserDefaults] boolForKey:@"beep"];
    BOOL isVirbOn  =[[NSUserDefaults standardUserDefaults] boolForKey:@"vibration"];
    BOOL isLedOn   =[[NSUserDefaults standardUserDefaults] boolForKey:@"led"];
    BOOL isIllumOn =[[NSUserDefaults standardUserDefaults] boolForKey:@"illumination"];
    
    [self.olBeep            setOn:isBeepOn];
    [self.olVibration       setOn:isVirbOn];
    [self.olLED             setOn:isLedOn];
    [self.olIllumination    setOn:isIllumOn];
    
    [self.olDefaultTriggerOn setOn:isDefaultTriggerOn];
    [self.olAutoPower        setOn:isAutoPower];
    
    
    CommonReaderInfo *info = [CommonReaderInfo sharedInstance];
    
    if(info.m_nCurrentSelectDevice != RCP_COMBO_DEVICE_BARCODE)
    {
        [self.olCellFactory setHidden: YES];
    }
    else
        [self.olCellFactory setHidden: NO];
    
    
}

-(void)getDeviceInfo
{
    //Common
    [self.m_Commdevice getReaderInfo:00];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)saveSettingVal
{
    
    [self.m_Commdevice setBeep:self.olBeep.isOn setVibration:self.olVibration.isOn setIllumination:self.olIllumination.isOn setLED:self.olLED.isOn];
    
    [[NSUserDefaults standardUserDefaults] setBool:self.olBeep.isOn forKey:@"beep"];
    [[NSUserDefaults standardUserDefaults] setBool:self.olIllumination.isOn forKey:@"illumination"];
    [[NSUserDefaults standardUserDefaults] setBool:self.olVibration.isOn forKey:@"vibration"];
    [[NSUserDefaults standardUserDefaults] setBool:self.olLED.isOn forKey:@"led"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) showAlert:(NSString*)strMsg
{
    if(self.mAlert == nil)
        self.mAlert = [[UIAlertView alloc]initWithTitle:nil message:@"" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    
    [self.mAlert setMessage:strMsg];
    [self.mAlert show];
}


- (void)resFactoryRset:(uint8_t)status
{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                   
                       switch (status)
                       {
                           case 0x00:
                           {
                               NSString *strMsg =@"Res : Doing Reset .... ";
                               [self showAlert:strMsg];
                            
                            }
                            break;
                               
                           case 0xFF:
                           {
                               NSString *strMsg =@"Res :  Compleate Factory Reset For Barcode";
                               [self showAlert:strMsg];
                           }
                               break;
                               
                           default:
                               break;
                       }

                   });

    
    
}


//Beep
- (IBAction)eBeepChanged:(id)sender
{
    [self saveSettingVal];
}


//Illumination
- (IBAction)eIlluminationChanged:(id)sender
{
    [self saveSettingVal];
}

//Vibration
- (IBAction)eVibrationChanged:(id)sender
{
    [self saveSettingVal];
}


//LED
- (IBAction)eLEDChanged:(id)sender
{
    [self saveSettingVal];
}


//Auto Power On /OFF
- (IBAction)eAutoPower:(id)sender
{
    UISwitch *sw = (UISwitch*)sender;
    [[NSUserDefaults standardUserDefaults] setBool:[sw isOn] forKey:@"AutoPowerOn"];
    
}


//Trigger
- (IBAction)eTriggerDefault:(id)sender
{
    UISwitch *sw = (UISwitch*)sender;
    [CommonDevice setTriggerModeDefault: [sw isOn]];
    
    [[NSUserDefaults standardUserDefaults] setBool:[sw isOn] forKey:@"DefaultTriggerOn"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


@end
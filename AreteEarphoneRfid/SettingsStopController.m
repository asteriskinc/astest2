//  SettingsStopController.m

#import "SettingsStopController.h"
#import "ModelSettings.h"

/*SDK header files*/
#import "ComboDevices.h"

@interface SettingsStopController ()
{
    
}
@property (nonatomic,retain)ModelSettings *m_Model;
@end

@implementation SettingsStopController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.m_Model = [ModelSettings sharedInstance];
    
    
    CommonReaderInfo *info = [CommonReaderInfo sharedInstance];
    int nTagCount =info.nCount;
    int nScanTime =info.nScanTime;
    int nCycle    =info.nCycle;
    
    
    self.olStopTagCount.text = [NSString stringWithFormat:@"%d",nTagCount];
    self.olStopTime.text     = [NSString stringWithFormat:@"%d",nScanTime];
    self.olStopCycle.text    = [NSString stringWithFormat:@"%d",nCycle];
    
   
}

- (IBAction)rightBarButtonItemClicked:(id)sender
{
    [self.olStopTagCount resignFirstResponder];
    [self.olStopTime resignFirstResponder];
    [self.olStopCycle resignFirstResponder];
       
    int stopTagCount =   (int)[self.olStopTagCount.text integerValue];
    int stopTime     =   (int)[self.olStopTime.text integerValue];
    int stopCycle    =   (int)[self.olStopCycle.text integerValue];
    
    
    [[NSUserDefaults standardUserDefaults] setInteger:stopTagCount forKey:@"RFIDScanTagCount"];
    [[NSUserDefaults standardUserDefaults] setInteger:stopTime forKey:@"RFIDScanTagTime"];
    [[NSUserDefaults standardUserDefaults] setInteger:stopCycle forKey:@"RFIDScanTagInventory"];
    
    CommonDevice *device = [CommonDevice sharedInstance];
    [device setTagCount:stopTagCount setSacnTime:stopTime setCycle:stopCycle];
    

    

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setOlStopTagCount:nil];
    [self setOlStopTime:nil];
    [self setOlStopCycle:nil];
    [super viewDidUnload];
}
@end

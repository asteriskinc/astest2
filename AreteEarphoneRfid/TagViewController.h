//  TagViewController.h

#import <UIKit/UIKit.h>
#import "ModelSettings.h"
@interface TagViewController : UITableViewController
{
    NSMutableArray *_accessoryList;
    ModelSettings *model;
}
-(void) updateData;
@end

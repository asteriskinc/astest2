//  SettingsFhLbtController.h

#import <UIKit/UIKit.h>
#import "protocols.h"
@interface SettingsFhLbtController : UIViewController <RcpRFIDDelegate>
@property (nonatomic,assign)NSInteger senseTime;
@property (nonatomic,assign)NSInteger lbtLevel;
@property (nonatomic,assign)NSInteger fhEnable;
@property (nonatomic,assign)NSInteger lbtEnable;
@property (nonatomic,assign)NSInteger cwEnable;
@property (weak, nonatomic) IBOutlet UITextField *olOnTime;
@property (weak, nonatomic) IBOutlet UITextField *olOffTime;
- (IBAction)rightBarButtonItemClicked:(id)sender;
@end

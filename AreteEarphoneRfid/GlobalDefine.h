//
//  GlobalDefine.h
//  Created by Robin on 2016. 1. 17..

#ifndef GlobalDefine_h
#define GlobalDefine_h

#import "Common.h"
#import "NSLoggerSupport.h"

//ERROR
#define df_ERROR   0
#define df_SUCESS  1


//Satus
#define df_BATTERY_CHARGING      2


//Setting VAl
#define df_SETTING_EMPTY         -1
#define df_SETTING_BEEP          @"BEEP"
#define df_SETTING_VIBRATION     @"VIBRATION"
#define df_SETTING_LED           @"LED"
#define df_SETTING_ILLUMINATION  @"ILLUMINATION"



//Cell NSDictionary
#define df_CELL_TAG_INFO         @"TAG_INFO"
#define df_CELL_TAG_COUNT        @"COUNT"
#define df_CELL_TAG_RAW          @"RAW"
#define df_CELL_TAG_DEVICETYPE   @"DEVICE"
#define df_CELL_TAG_RSSI         @"RSSI"



//ReaderInfoType

#define df_READER_INFO_FROM_FIRMWARE   0xB1  // Request Reader Info From  Firmware
#define df_READER_INFO_FROM_RFID       0xB0  // Reqeust Reader Info From  Module



//Region
#define REGION_KOREA_OLD			0x01
#define REGION_USA_OLD 				0x02
#define REGION_EUROPE_OLD			0x04
#define REGION_JAPAN_OLD			0x08
#define REGION_CHINA2_OLD			0x10
#define REGION_CHINA1_OLD			0x16
#define REGION_USA2_OLD				0x32

#define REGION_KOREA				0x11
#define REGION_USA 					0x21
#define REGION_USA2	 				0x22
#define REGION_EUROPE				0x31
#define REGION_JAPAN				0x41
#define REGION_CHINA1				0x51
#define REGION_CHINA2				0x52
#define REGION_BRAZIL1              0x61
#define REGION_BRAZIL2              0x62
#define REGION_AU_HK				0x71


//Reset Type
#define RCP_RESET_TYPE_HW                 0x01  //Notification when select HW Reset Button
#define RCP_RESET_TYPE_APP                0x00  //Notification when slecct App Reset Button
#define RCP_RESET_TYPE_NONE               0xF0
#endif /* GlobalDefine_h */











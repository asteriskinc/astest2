//  AreteEarphoneRfidViewController.m
//  Created by Robin on 16. 8. 22..


#import "AreteRfidViewController.h"
#import "TagViewController.h"
#import "settingsController.h"
#import "GlobalDefine.h"

/*SDK header files*/
#import "NFCCommandDefine.h"
#import "ComboDevices.h"

@interface AreteRfidViewController()
{
    int mBatteryVal;
    BOOL mbIsCahrging;
    NSTimer *timer;
    int readCount;
}
@property (weak, nonatomic) IBOutlet UILabel                *olLabelConnectState;
@property (weak, nonatomic) IBOutlet UIBarButtonItem        *olBtnSettings;
@property (weak, nonatomic) IBOutlet UILabel                *olTagCount;
@property (weak, nonatomic) IBOutlet UISegmentedControl     *olModeSeg;
@property (weak, nonatomic) IBOutlet UIImageView            *olBattery;
@property (weak, nonatomic) IBOutlet UIButton               *olBtnRead;
@property (weak, nonatomic) IBOutlet UIButton               *olBtnClear;
@property (weak, nonatomic) IBOutlet UIButton               *olBtnStop;
@property (weak, nonatomic) IBOutlet UISwitch               *olSwitch;
@property (nonatomic, strong, readwrite) TagViewController  *tagViewController;
@property (nonatomic, strong, readwrite) SettingsController *settingsController;
@property (nonatomic, retain) ModelSettings                 *m_Model;
@property (nonatomic, retain) CommonDevice                  *m_DeviceCommon;
@property (weak, nonatomic) IBOutlet UITextField *startText;
@property (weak, nonatomic) IBOutlet UITextField *stopText;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (nonatomic, strong) NSString *startTime;
@property (nonatomic, strong) NSString *readedTag;


-(void) updateScanData;
- (IBAction)actionSwitch:(UISwitch *)sender;
- (IBAction)btnRead:(UIButton *)sender;
- (IBAction)btnClear:(UIButton *)sender;
- (IBAction)btnStop:(UIButton *)sender;
- (IBAction)actionModeSeg:(id)sender;
- (IBAction)btnNFCRawdataSend:(id)sender;
@end


@implementation AreteRfidViewController

#pragma mark - segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segueTagTableView"])
    {
        self.tagViewController = segue.destinationViewController;
    }
    else if ([segue.identifier isEqualToString:@"segueSettings"])
    {
        self.settingsController = segue.destinationViewController;
    }
    
}


/***********************************************************************/
#pragma mark View life
/***********************************************************************/

- (void)viewDidLoad
{
    [super viewDidLoad];
    if(![[NSUserDefaults standardUserDefaults] boolForKey:@"isSavedData"])
    {
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"AutoPowerOn"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"beep"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"illumination"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"vibration"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"led"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isSavedData"];
        [[NSUserDefaults standardUserDefaults] setInteger:0  forKey:@"RFIDScanTagCount"];
        [[NSUserDefaults standardUserDefaults] setInteger:0  forKey:@"RFIDScanTagTime"];
        [[NSUserDefaults standardUserDefaults] setInteger:0  forKey:@"RFIDScanTagInventory"];
        [[NSUserDefaults standardUserDefaults] setInteger:0  forKey:@"RFIDEncoding"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"RSSIOn"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"DefaultTriggerOn"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    mBatteryVal = 0;
    readCount = 0;
    mbIsCahrging = NO;
    int nTagCount =(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"RFIDScanTagCount"];
    int nScanTime =(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"RFIDScanTagTime"];
    int nCycle    =(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"RFIDScanTagInventory"];
    
    BOOL isDefaultTriggerOn  = [[NSUserDefaults standardUserDefaults] boolForKey:@"DefaultTriggerOn"];
    [CommonDevice setTriggerModeDefault:isDefaultTriggerOn];
   
    
    CommonDevice *device = [CommonDevice sharedInstance];
    [device setTagCount:nTagCount setSacnTime:nScanTime setCycle:nCycle];
    
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(batteryLevelChanged:)
                                                name:UIDeviceBatteryStateDidChangeNotification
                                              object:nil];
    
    
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.m_Model         = [ModelSettings sharedInstance];
    self.m_DeviceCommon  = [CommonDevice sharedInstance];
    
    self.m_DeviceCommon.delegateCommon  = self;
    self.m_DeviceCommon.delegateRFID    = self;
    self.m_DeviceCommon.delegateHWEvent = self;
    
    // when Anohter view changed mode by H/W mode change
    [self  updateScanData];
    [UIDevice currentDevice].batteryMonitoringEnabled = YES;
    
    
}



-(void)batteryLevelChanged:(NSNotification *)notification
{
    
    UIDevice* currentDevice = [UIDevice currentDevice];
    UIDeviceBatteryState currentState = [currentDevice batteryState];
    
    if ((currentState == UIDeviceBatteryStateCharging) || (currentState == UIDeviceBatteryStateFull)) {
        mbIsCahrging = YES;
    }
    else
        mbIsCahrging = NO;
    
    NSLog(@"BatState");
    
    
    int nVal = 0;
    if(mBatteryVal > 95)
        nVal = 4;
    else if(mBatteryVal  >70)
        nVal =3;
    else if(mBatteryVal  >45)
        nVal =2;
    else if(mBatteryVal  > 20)
        nVal =1;
    else
        nVal =0;
    
    NSString *imageName;
    
    if (mbIsCahrging)
        imageName =[NSString stringWithFormat:@"bat_charge_%d.png", nVal];
    else
        imageName =[NSString stringWithFormat:@"bat_normal_%d.png", nVal];
    
    self.olBattery.image = [UIImage imageNamed:imageName];
    
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    [UIDevice currentDevice].batteryMonitoringEnabled = NO;
    [super viewDidDisappear:YES];
    [self btnStop:(UIButton*)self];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


/***********************************************************************/
#pragma mark Delegate
/***********************************************************************/


// Battery info //STEP : 0 , 25, 50 , 75, 100
- (void)adcReceived:(NSData*)data
{
    
    Byte *b    = (Byte*) [data bytes];
    int adc    = b[0];
    
    
    int  nVal         =   (int)adc;
    mBatteryVal =  nVal;
    
    if(nVal > 95)
        nVal = 4;
    else if(nVal  >70)
        nVal =3;
    else if(nVal  >45)
        nVal =2;
    else if(nVal  > 20)
        nVal =1;
    else
        nVal =0;
    
    NSLog(@"Battery %d",adc);
    
    NSString *imageName;
    
    if (mbIsCahrging)
        imageName =[NSString stringWithFormat:@"bat_charge_%d.png", nVal];
    else
        imageName =[NSString stringWithFormat:@"bat_normal_%d.png", nVal];
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       
                       self.olBattery.image = [UIImage imageNamed:imageName];
                   });
    
}

//Plug info
- (void)plugged:(BOOL)plug
{
    if(plug)
    {
         NSLog(@"INFO + plugedState");
        [self plugedState];
    
    }
    else
    {
         NSLog(@"INFO + unPlugedState");
        [self unPlugedState];
    }
}




//Connected Info
- (void)readerConnected:(uint8_t)status
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       switch (status)
                       {
                           case 0xff:
                               NSLog(@"INFO + CONNECTED");
                               [self connectedState];
                               break;
                               
                               
                           default:
                               NSLog(@"INFO + DISCONNECTED");
                               [self disConnectedState];
                               break;
                               
                       }
                   });
}


// recv Scan Data
- (void)receivedScanData:(NSData *)readData DeviceType:(int)nDeviceType
{
    
    NSMutableString* tag = [[NSMutableString alloc]init] ;
    unsigned char* ptr= (unsigned char*) [readData bytes];
    
    switch (nDeviceType)
    {
        case RCP_COMBO_DEVICE_BARCODE:
            tag = [[NSMutableString alloc] initWithData:readData encoding:NSShiftJISStringEncoding];
            
            NSLog(@"[BARCODE] : %@",tag);
            break;
            
        case RCP_COMBO_DEVICE_RFID:
            for(int i = 0; i < readData.length; i++)
                [tag appendFormat:@"%02x", *ptr++ & 0xFF ];
            break;
            
        case RCP_COMBO_DEVICE_NFC:
        {
            // NFC DATA FORMAT : STX(1) - ADDR(1) - COMMAND(1) - LEN(1)  - DATA( XX) - ETX(1) - SUM(1) - CR(1)
#define df_NFCDATA_HEADER_SIZE 4
#define df_NFCDATA_FOOTER_SIZE 3
            
            int nNFCDataLen = (int)ptr[3];
            int nTotalSize  = nNFCDataLen+df_NFCDATA_HEADER_SIZE+df_NFCDATA_FOOTER_SIZE;
            
            /* Check Sum */
            Byte btSum = 0x00;
            for(int i = 0; i<nNFCDataLen+df_NFCDATA_HEADER_SIZE+1; i++)
            {
                btSum = (Byte)(btSum+ ptr[i]);
            }
            
            
            //Size Check
            if(nTotalSize != (int)readData.length)
            {
                [tag appendFormat:@"Data Size Err"];
            }
            //Check Sum
            else if(btSum != ptr[df_NFCDATA_HEADER_SIZE+nNFCDataLen+1])
            {
                [tag appendFormat:@"Check Sum Err"];
            }
            //Normal NFC Data
            else if((ptr[0] == 0x02)&&(ptr[df_NFCDATA_HEADER_SIZE+nNFCDataLen] == 0x03)&&(nTotalSize== readData.length))
            {
                for(int i = 0; i<nNFCDataLen; i++)
                    [tag appendFormat:@"%02x", ptr[i+4] & 0xFF ];
            }
            else
            {
                [tag appendFormat:@"Data Format Err"];
            }
            
        }
            break;
            
        default:
            [tag appendFormat:@"Not Found Device Type"];
            break;
    }
    NSData *data = [[NSData alloc]initWithData:readData];
    [self addScanDataFiltering:tag DeviceType:nDeviceType RawData:data RSSI:0];
    
}

// Scan Data Filtering and Manage Data
-(void)addScanDataFiltering:(NSString*)strScanRead DeviceType:(int)nDeviceType RawData:(NSData*)dataRaw RSSI:(int)nRSSI
{
    
    BOOL isNewData = TRUE;
    
    NSString *strDeviceType = [NSString stringWithFormat:@"%d",nDeviceType];
    
    NSString *strRSSI;
    
    if(nRSSI!=0)
        strRSSI =  [NSString stringWithFormat:@"%d",nRSSI];
    else
        strRSSI = @"";
    
    
    for(int i=0; i<self.m_Model.m_arReadTagData.count;i++)
    {
        NSDictionary *comDic    = [self.m_Model.m_arReadTagData objectAtIndex:i];
        NSString *strCompare    = [comDic objectForKey:df_CELL_TAG_INFO];
        
        if ([strCompare isEqualToString:strScanRead])
        {
            int nCount = [[comDic objectForKey:df_CELL_TAG_COUNT] intValue]+1;
            NSString *strCount = [NSString stringWithFormat:@"%d",nCount];
            
            
            NSDictionary *inserDic  = [NSDictionary dictionaryWithObjectsAndKeys:
                                       strScanRead, df_CELL_TAG_INFO,
                                       strCount, df_CELL_TAG_COUNT,
                                       dataRaw,df_CELL_TAG_RAW,
                                       strRSSI,df_CELL_TAG_RSSI,
                                       strDeviceType,df_CELL_TAG_DEVICETYPE
                                       ,nil];
            [self.m_Model.m_arReadTagData replaceObjectAtIndex:i withObject:inserDic];
            isNewData = FALSE;
            break;
        }
    }
    
    
    if(isNewData)
    {
        //[self playSound:0];
        NSString *strCount = @"1";
        NSDictionary *inserDic  = [NSDictionary dictionaryWithObjectsAndKeys:
                                   strScanRead, df_CELL_TAG_INFO,
                                   strCount, df_CELL_TAG_COUNT,
                                   dataRaw,df_CELL_TAG_RAW,
                                   strRSSI,df_CELL_TAG_RSSI,
                                   strDeviceType,df_CELL_TAG_DEVICETYPE
                                   ,nil];
        
        [self.m_Model.m_arReadTagData addObject:inserDic];
    }
    
    [self  updateScanData];
}

// recv  Scand Data include RSSI Data
#pragma mark - RcpRFIDDelegate Method
- (void)pcEpcRssiReceived:(NSData *)pcEpc rssi:(int8_t)rssi
{
    NSMutableString* tag = [[NSMutableString alloc]init] ;
    unsigned char* ptr= (unsigned char*) [pcEpc bytes];
    
    for(int i = 0; i < pcEpc.length; i++)
        [tag appendFormat:@"%02x", *ptr++ & 0xFF ];
    
    [self addScanDataFiltering:tag DeviceType:RCP_COMBO_DEVICE_RFID RawData:pcEpc RSSI:rssi];
    
}


/***********************************************************************/
#pragma mark IBAction
/***********************************************************************/


-(void) setConnectCommand:(BOOL)isConnect
{
    
    int nSectedType = (int)self.olModeSeg.selectedSegmentIndex;
    BOOL isBeepOn  =[[NSUserDefaults standardUserDefaults] boolForKey:@"beep"];
    BOOL isVirbOn  =[[NSUserDefaults standardUserDefaults] boolForKey:@"vibration"];
    BOOL isLedOn   =[[NSUserDefaults standardUserDefaults] boolForKey:@"led"];
    BOOL isIllumOn =[[NSUserDefaults standardUserDefaults] boolForKey:@"illumination"];
    [[NSUserDefaults standardUserDefaults] setInteger: self.olModeSeg.selectedSegmentIndex  forKey:@"SelectedMode"];
    
    [self.m_DeviceCommon setReaderPower:isConnect
                                 buzzer:isBeepOn
                              vibration:isVirbOn
                                    led:isLedOn
                           illumination:isIllumOn
                                   mode:nSectedType];
    
    
}



- (IBAction)actionSwitch:(UISwitch *)sender
{
    UISwitch *sw = (UISwitch*)sender;
    [self setConnectCommand:sw.isOn];
    if (!sw.isOn) {
        if([timer isValid]) [timer invalidate];
    }
}

- (IBAction)btnRead:(UIBarButtonItem *)sender
{
    
    CommonReaderInfo *info = [CommonReaderInfo sharedInstance];
    int nTagCount =   info.nCount;
    int nScanTime =   info.nScanTime;
    int nCycle    =   info.nCycle;
    
    BOOL bIsRSSIOn            = [[NSUserDefaults standardUserDefaults] boolForKey:@"RSSIOn"];
    
    int nSelectedDevice = (int)self.olModeSeg.selectedSegmentIndex;
    
    //RFID
    if(nSelectedDevice == RCP_COMBO_DEVICE_RFID)
    {
        ComboRFIDApi *device = [ComboRFIDApi sharedInstance];
        if(bIsRSSIOn)
            [device startReadTagsWithRssi:nTagCount mtime:nScanTime repeatCycle:nCycle];
        else
            [device startScan:nTagCount mtime:nScanTime repeatCycle:nCycle];
    }
    
    // NFC
    else if(nSelectedDevice == RCP_COMBO_DEVICE_NFC)
    {
        
        ComboNFCApi *device = [ComboNFCApi sharedInstance];
        [device startScan];
        
    }
    
    // BARCODE
    else if(nSelectedDevice == RCP_COMBO_DEVICE_BARCODE)
    {
        ComboBarcodeApi *device = [ComboBarcodeApi sharedInstance];
        [device startScan];
    }
    
    
    float onTime = 0;
    if (![self.startText.text isEqualToString:@""]) {
        onTime = self.startText.text.floatValue;
    }
    
    timer = [NSTimer timerWithTimeInterval:onTime target:self selector:@selector(stopTimer:) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    
      // NsDate => NSString変換用のフォーマッタを作成
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"]]; // Localeの指定
    [df setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    
    // 日付(NSDate) => 文字列(NSString)に変換
    NSDate *now = [NSDate date];
    NSString *strNow = [df stringFromDate:now];
    
    // ログ出力
    NSLog(@"%d回目 %@ Failed",readCount,strNow);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        readCount++;
        _countLabel.text = [NSString stringWithFormat:@"%d",readCount];
        [self writeLog];
    });
    
    dispatch_async(dispatch_get_main_queue(), ^{
        //            readCount++;
        
        NSDateFormatter *df = [[NSDateFormatter alloc] init];
        [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"]]; // Localeの指定
        [df setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
        NSDate *now = [NSDate date];
        self.startTime = [df stringFromDate:now];
    });
    
}

- (void)stopTimer:(id)sender{
    
    int nSelectedDevice = (int)self.olModeSeg.selectedSegmentIndex;
    
    //RFID
    if(nSelectedDevice == RCP_COMBO_DEVICE_RFID){
        ComboRFIDApi *device = [ComboRFIDApi sharedInstance];
        [device stopScan];
    }
    
    // NFC
    else if(nSelectedDevice == RCP_COMBO_DEVICE_NFC){
        ComboNFCApi *device = [ComboNFCApi sharedInstance];
        [device stopScan];
    }
    
    // BARCODE
    else if(nSelectedDevice == RCP_COMBO_DEVICE_BARCODE){
        ComboBarcodeApi *device = [ComboBarcodeApi sharedInstance];
        [device stopScan];
    }
    
    if([timer isValid]) [timer invalidate];
    
    float offTime = 0;
    if (![self.stopText.text isEqualToString:@""]) {
        offTime = self.stopText.text.floatValue;
    }
    
    timer = [NSTimer timerWithTimeInterval:offTime target:self selector:@selector(startTimer:) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

- (void)startTimer:(id)sender{
    
    int stopTagCount = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"stopTagCount"];
    int stopTime = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"stopTime"];
    int stopCycle = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"stopCycle"];
    
    int nSelectedDevice = (int)self.olModeSeg.selectedSegmentIndex;
    
    //RFID
    if(nSelectedDevice == RCP_COMBO_DEVICE_RFID){
        ComboRFIDApi *device = [ComboRFIDApi sharedInstance];
        [device startScan:stopTagCount mtime:stopTime repeatCycle:stopCycle];
    }
    
    // NFC
    else if(nSelectedDevice == RCP_COMBO_DEVICE_NFC){
        ComboNFCApi *device = [ComboNFCApi sharedInstance];
        [device startScan];
    }
    
    // BARCODE
    else if(nSelectedDevice == RCP_COMBO_DEVICE_BARCODE){
        ComboBarcodeApi *device = [ComboBarcodeApi sharedInstance];
        [device startScan];
    }
    
    // NsDate => NSString変換用のフォーマッタを作成
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"]]; // Localeの指定
    [df setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    
    // 日付(NSDate) => 文字列(NSString)に変換
    NSDate *now = [NSDate date];
    NSString *strNow = [df stringFromDate:now];
    
    // ログ出力
    NSLog(@"%d回目 %@ Failed",readCount,strNow);
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        readCount++;
        _countLabel.text = [NSString stringWithFormat:@"%d",readCount];
        [self writeLog];
    });
    
    if([timer isValid]) [timer invalidate];
    float onTime = 0;
    if (![self.stopText.text isEqualToString:@""]) {
        onTime = self.stopText.text.floatValue;
    }
    timer = [NSTimer timerWithTimeInterval:onTime target:self selector:@selector(stopTimer:) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}

- (void)writeLog{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"ja_JP"]]; // Localeの指定
    [df setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
    NSDate *now = [NSDate date];
    self.startTime = [df stringFromDate:now];
    
    // ログ出力
    NSLog(@"%@ ：%@",_startTime,_readedTag);
    _readedTag = @"";
    _startTime = @"";
}

- (IBAction)btnClear:(UIBarButtonItem *)sender
{
    [self.m_Model.m_arReadTagData removeAllObjects];
    [self  updateScanData];
    self.countLabel.text = @"0";
    readCount = 0;
}

- (IBAction)btnStop:(UIBarButtonItem *)sender
{
    
    int nSelectedDevice = (int)self.olModeSeg.selectedSegmentIndex;
    
    if(nSelectedDevice == RCP_COMBO_DEVICE_RFID)
    {
        ComboRFIDApi *device = [ComboRFIDApi sharedInstance];
        [device stopScan];
    }
    // NFC
    else if(nSelectedDevice == RCP_COMBO_DEVICE_NFC)
    {
        ComboNFCApi *device = [ComboNFCApi sharedInstance];
        [device stopScan];
    }
    
    // 2D
    else if(nSelectedDevice == RCP_COMBO_DEVICE_BARCODE)
    {
        ComboBarcodeApi *device = [ComboBarcodeApi sharedInstance];
        [device stopScan];
    }
    
    if([timer isValid]) [timer invalidate];
}

- (IBAction)actionModeSeg:(id)sender
{
    [self setConnectCommand:NO];
}

- (IBAction)close:(id)sender {
    [self.startText resignFirstResponder];
    [self.stopText resignFirstResponder];
}


/***********************************************************************/
#pragma mark UI Update
/***********************************************************************/

-(void) plugedState
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       self.olSwitch.hidden               = NO;
                       CommonReaderInfo *info              = [CommonReaderInfo sharedInstance];
                       
                       /* Read Save Last connected Device Info*/
                       int nAutoPowerDevice =(int)[[NSUserDefaults standardUserDefaults] integerForKey:@"SelectedMode"];
                       
                       if(info.bIsPowerOn)
                            [self.olLabelConnectState setText:@" Connected "];
                        else
                            [self.olLabelConnectState setText:@" Disconnected "];
                       
                       [self.olSwitch setOn:NO];
                       [self setTitle:info.strName];
                       
                       
                       if(info.bCanUseBarcode)
                           [self.olModeSeg setEnabled:YES  forSegmentAtIndex:0];
                       else
                           [self.olModeSeg setEnabled:NO  forSegmentAtIndex:0];
                       
                       
                       if(info.bCanUseRFID)
                           [self.olModeSeg setEnabled:YES  forSegmentAtIndex:1];
                       else
                           [self.olModeSeg setEnabled:NO  forSegmentAtIndex:1];
                       
                       
                       if(info.bCanUseNFC)
                           [self.olModeSeg setEnabled:YES  forSegmentAtIndex:2];
                       else
                           [self.olModeSeg setEnabled:NO  forSegmentAtIndex:2];
                       
                       
                       int nSelectDevice = nAutoPowerDevice;
                       
                       //0230D Check
                       if((info.bCanUseBarcode)&&(info.bCanUseRFID))
                       {
                           if(nAutoPowerDevice == RCP_COMBO_DEVICE_NFC)
                               nSelectDevice = RCP_COMBO_DEVICE_BARCODE;
                       }
                       //0240D Check
                       else if((info.bCanUseBarcode)&&(info.bCanUseNFC))
                       {
                           if(nAutoPowerDevice == RCP_COMBO_DEVICE_RFID)
                               nSelectDevice = RCP_COMBO_DEVICE_BARCODE;
                           
                       }
                       else if(info.bCanUseBarcode)
                           nSelectDevice = RCP_COMBO_DEVICE_BARCODE;
                       else if(info.bCanUseRFID)
                           nSelectDevice = RCP_COMBO_DEVICE_RFID;
                       else if(info.bCanUseNFC)
                           nSelectDevice = RCP_COMBO_DEVICE_NFC;
                       
                       [self.olModeSeg setSelectedSegmentIndex:nSelectDevice];
                       
                       BOOL isAutoPower = [[NSUserDefaults standardUserDefaults] boolForKey:@"AutoPowerOn"];
                       if( isAutoPower)
                       {
                           [_olSwitch setOn:YES];
                           [self actionSwitch:_olSwitch];
                       }
                   });
    
}

-(void) unPlugedState
{
    
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       [self disConnectedState];
                       [self.olLabelConnectState setText:@" Disconnected "];
                       [self.olSwitch setOn:NO];
                       [self.olSwitch setHidden:YES];
                       [self setTitle:@"UnknowDevice"];
                       [self.olModeSeg setEnabled:NO  forSegmentAtIndex:0];
                       [self.olModeSeg setEnabled:NO  forSegmentAtIndex:1];
                       [self.olModeSeg setEnabled:NO  forSegmentAtIndex:2];
                       [self.olModeSeg setSelectedSegmentIndex:0];
                   });
}


-(void) connectedState
{
    [self.olLabelConnectState setTextColor:[UIColor blueColor]];
    [self.olLabelConnectState setText:@" Connected"];
    if(![self.olSwitch isOn])
        [self.olSwitch setOn:YES];
    
    self.olSwitch.hidden        = NO;
    self.olBtnRead.hidden       = NO;
    self.olBtnClear.hidden      = NO;
    self.olBtnStop.hidden       = NO;
    self.olBtnSettings.enabled  = YES;
    
}

-(void) disConnectedState
{
    [self.olLabelConnectState setTextColor:[UIColor redColor]];
    [self.olLabelConnectState setText:@" Disconnected"];
    
    if([self.olSwitch isOn])
        [self.olSwitch setOn:NO];
    
    self.olSwitch.hidden        = NO;
    self.olBtnRead.hidden       = YES;
    self.olBtnClear.hidden      = YES;
    self.olBtnStop.hidden       = YES;
    self.olBtnSettings.enabled  =  NO;
}



-(void) updateScanData
{
    NSLog(@"INFO +  TAG COUNT : %d",(int)self.m_Model.m_arReadTagData.count);
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       [self.tagViewController updateData];
                       self.olTagCount.text = [NSString stringWithFormat:@"%d",(int)self.m_Model.m_arReadTagData.count];

                   });
    }



/***********************************************************************/
#pragma mark NFC Raw Send / Recieve
/***********************************************************************/

-(IBAction)btnNFCRawdataSend:(id)sender
{
    //Ex ) Inventory Mode set
    ComboNFCApi *device  = [ComboNFCApi sharedInstance];
    Byte cmd[]           = df_NFC_CMD_INVENTORYSET;
    [device sendRawData:cmd len:sizeof(cmd)];
}


- (void)nfcRawDataReceived:(NSData *)rawData
{
    NSLog(@"NFC RAW DATA Receive");
    // you can reiceived command about NFC type
}

- (void)allRawDataReceived:(NSData *)rawData
{
    // you can reiceived command about ALL type
}


/***********************************************************************/
#pragma mark Commbo HW Event
/***********************************************************************/
- (void)resPowerOnOff:(BOOL)isOn Device:(int)nDeviceType IsHWModeChange:(BOOL)bIsHWModeChange
{
    
    //Last selected Divece saved 
    [[NSUserDefaults standardUserDefaults] setInteger: self.olModeSeg.selectedSegmentIndex  forKey:@"SelectedMode"];
    
    if(isOn)
    {
        /* NFC Initial */
        if( nDeviceType  == RCP_COMBO_DEVICE_NFC)
        {
            ComboNFCApi *nfcDevice = [ComboNFCApi sharedInstance];
            [NSThread sleepForTimeInterval:1.2];
            [nfcDevice setInventory:1.0];
        }
        
    }
    
    if(bIsHWModeChange)
    {
        dispatch_async(dispatch_get_main_queue(),
                       ^{
                           [self.olModeSeg setSelectedSegmentIndex:nDeviceType];
                       });
    }
    
}


- (void)pushedTriggerButton
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       [self setTitle:@"Custom TriggerDown"];
                   });
    
    
}

- (void)releasedTriggerButton
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       [self setTitle:@"Custom TriggerDownUp"];
                   });
    
}

- (void)checkTriggerStatus:(NSString*)strStatus
{
    dispatch_async(dispatch_get_main_queue(),
                   ^{
                       [NSThread sleepForTimeInterval:0.05f];
                       [self setTitle:strStatus];
                   });
    
}



@end




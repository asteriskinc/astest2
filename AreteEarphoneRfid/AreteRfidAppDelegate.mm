

#import "AreteRfidAppDelegate.h"
#import "CommonDevice.h"
#import "ModelSettings.h"
#import "PhyUtility.h"



@implementation AreteRfidAppDelegate

//@synthesize window = _window;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    [UIApplication redirectConsoleLogToDocumentFolder:@"log.txt"];
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [NSLoggerSupport startLogger];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    NSLog(@"applicationWillResignActive");
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"applicationDidEnterBackground");
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    NSLog(@"applicationWillEnterForeground");
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
//    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    NSLog(@"applicationDidBecomeActive");
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    NSLog(@"applicationWillTerminate");
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(buttonIndex == 0)
    {
       
        
    }
    else if(buttonIndex == 1)
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://dl.dropboxusercontent.com/s/w64pvmubtdeh6o9/download.html"]];
    }
}


@end

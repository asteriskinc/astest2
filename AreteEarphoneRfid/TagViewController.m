//
//  TagViewController.m

#import "TagViewController.h"
#import "TagAccessController.h"
#import "GlobalDefine.h"

#import "CustomCell.h"
#import "EpcConverter.h"

/*SDK header files*/
#import "typeDefine.h"
#import "BarcodeViewController.h"
#import "TagAccessController.h"


@interface TagViewController ()
@property (nonatomic, strong, readwrite) TagAccessController *tagAccessController;
@end

@implementation TagViewController
@synthesize tagAccessController = _tagAccessController;

-(void) updateData
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    model = [ModelSettings sharedInstance];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [model.m_arReadTagData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    CustomCell      *cell           = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSDictionary    *dic            = [model.m_arReadTagData objectAtIndex:[indexPath row]];
    cell.tagHex.text                = [dic objectForKey:df_CELL_TAG_INFO];
    cell.tagCount.text              = [dic objectForKey:df_CELL_TAG_COUNT];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    cell.tagRSSI.hidden = YES;
    cell.tagEncoding.hidden = YES;
    int nDeviceType = [[dic objectForKey:df_CELL_TAG_DEVICETYPE]intValue];
    
    switch (nDeviceType)
    {
            
        case RCP_COMBO_DEVICE_RFID:
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            NSString *strRSSI  =  [dic objectForKey:df_CELL_TAG_RSSI];
            
            
            BOOL isRSSIOn =  [[NSUserDefaults standardUserDefaults] boolForKey:@"RSSIOn"];
            if((isRSSIOn)&&(strRSSI.length>0))
            {
              cell.tagRSSI.hidden  = NO;
              cell.tagRSSI.text    = [NSString stringWithFormat:@" RSSI : %@",strRSSI];
            }
            
            cell.tagEncoding.hidden = NO;
            cell.tagEncoding.text  = [NSString stringWithFormat:@" Encoding : %@",[self getEncodingString:[dic objectForKey:df_CELL_TAG_RAW]]];
            cell.imgType.image              = [UIImage imageNamed:@"icon_rfid"];
        }
            break;
            
        case RCP_COMBO_DEVICE_NFC:
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.tagEncoding.text = [NSString stringWithFormat:@" Encoding : %@",[self getEncodingString:[dic objectForKey:df_CELL_TAG_RAW]]];
            cell.imgType.image    = [UIImage imageNamed:@"icon_nfc"];
        }
            break;
            
        case RCP_COMBO_DEVICE_BARCODE :
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.imgType.image              = [UIImage imageNamed:@"icon_barcode"];
            break;
        default:
            break;
    }
    
    return cell;
}


-(NSString *)getEncodingString:(NSData*)pcEpc
{

    int encoding_type =  (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"RFIDEncoding"];
    NSString *tag = [EpcConverter toString:encoding_type data:pcEpc];
    return tag;
}



#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    model.m_nSelectTagNum       = (int)[indexPath row];
    NSDictionary    *dic        = [model.m_arReadTagData objectAtIndex:[indexPath row]];
    int nDeviceType             = [[dic objectForKey:df_CELL_TAG_DEVICETYPE]intValue];
    

    BarcodeViewController *bar = [[UIStoryboard storyboardWithName:@"MainStoryboard_iphone" bundle:nil] instantiateViewControllerWithIdentifier:@"ID_BarcodeDetailView"];
    
    
    BarcodeViewController *rfid = [[UIStoryboard storyboardWithName:@"MainStoryboard_iphone" bundle:nil] instantiateViewControllerWithIdentifier:@"ID_TagAcessView"];
    
    
    if(nDeviceType == RCP_COMBO_DEVICE_RFID)
        [self.navigationController pushViewController:rfid animated:YES];
    else if(nDeviceType == RCP_COMBO_DEVICE_BARCODE)
        [self.navigationController pushViewController:bar animated:YES];
    
}

- (void)viewDidUnload {
    [self setTableView:nil];
    [super viewDidUnload];
}
@end

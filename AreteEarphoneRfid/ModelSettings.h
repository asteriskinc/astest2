//  ModelSettings.h
//  Created by Robin on 2016. 2. 22..

#import <Foundation/Foundation.h>
#define df_ENCODINGTYPE_HEX     0
#define df_ENCODINGTYPE_ASCII   1
#define df_ENCODINGTYPE_SGTIN96 2
#define df_ENCODINGTYPE_EAN13   3


@interface RFIDDeviceInfo : NSObject
@property(assign) int    m_nAntiMode;
@property(assign) int    m_nRFID_RF_CHANNEL;
@property(assign) float  m_fRFIDPower;
@end


@interface ModelSettings : NSObject
+ (id)  sharedInstance;

@property(nonatomic,retain) RFIDDeviceInfo *m_RFIDDeviceInfo;
@property(assign) int  m_nSelectTagNum;
@property(nonatomic,retain)NSMutableArray *m_arReadTagData;


@end

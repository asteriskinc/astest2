////  RFIDApi.h
////  Created by Robin on 2016. 4. 22..


#define df_ENGINEER_MODE

#import "SDeviceApi.h"

@interface ComboRFIDApi : SDeviceApi
+ (id)  sharedInstance;
- (void)setReaderType;
- (BOOL)startScan:(uint8_t)mtnu mtime:(uint8_t)mtime repeatCycle:(uint16_t)repeatCycle;
- (BOOL)stopScan;
- (BOOL)startReadTagsWithRssi:(uint8_t)maxTags mtime:(uint8_t)maxTime repeatCycle:(uint16_t)repeatCycle;
- (BOOL)startReadTagsWithTid:(uint8_t)mtnu mtime:(uint8_t)mtime repeatCycle:(uint16_t)repeatCycle;
- (BOOL)getSelectParam;
- (BOOL)setSelectParam:(uint8_t)target
                action:(uint8_t)action
            memoryBank:(uint8_t)memoryBank
               pointer:(uint32_t)pointer
                length:(uint8_t)length
              truncate:(uint8_t)truncate
                  mask:(NSData *)mask;

- (BOOL)getChannel;

- (BOOL)setChannel:(uint8_t)channel
     channelOffset:(uint8_t)channelOffset;

- (BOOL)getFhLbtParam;
- (BOOL)setFhLbtParam:(uint16_t)ReadTime
             idleTime:(uint16_t)IdleTime
     carrierSenseTime:(uint16_t) carrierSenseTime
              rfLevel:(uint16_t)rfLevel
     frequencyHopping:(uint8_t)frequencyHopping
     listenBeforeTalk:(uint8_t)listenBeforeTalk
       continuousWave:(uint8_t)continuousWave;


- (BOOL)getOutputPowerLevel;
- (BOOL)setOutputPowerLevel:(uint16_t)power;
- (BOOL)readFromTagMemoryLong:(uint32_t)accessPassword
                          epc:(NSData*)epc
                   memoryBank:(uint8_t)memoryBank
                 startAddress:(uint16_t)startAddress
                   dataLength:(uint16_t)dataLength;


- (BOOL)writeToTagMemory:(uint32_t)accessPassword
                     epc:(NSData*)epc
              memoryBank:(uint8_t)memoryBank
            startAddress:(uint16_t)startAddress
             dataToWrite:(NSData*)dataToWrite;



- (BOOL)killTag:(uint32_t)killPassword
            epc:(NSData*)epc;


- (BOOL)lockTagMemory:(uint32_t)accessPassword
                  epc:(NSData*)epc
             lockData:(uint32_t)lockData;


- (BOOL)getFreqHoppingTable;


- (BOOL)setFreqHoppingTable:(uint8_t)tableSize
                   channels:(NSData*)channels;


- (BOOL)getRssi;
- (BOOL)getSession;
- (BOOL)setSession:(uint8_t)session;
- (BOOL)getAnticollision;
//- (BOOL)setAnticollision:(uint8_t)mode;
- (BOOL)setAnticollision:(uint8_t)mode Counter:(uint8_t)counter;
- (BOOL)updateRegistry;
- (BOOL)getRFIDOnOffTime;


-(BOOL)setStopConditionTagNum:(Byte)mtnu Time:(Byte) mtime Recycle:(UInt16)repeatCycle;

- (BOOL)getRFIDMoudleVersion;



//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Funtions  for Engineer  Or  Testing Funtion
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////



#ifdef df_ENGINEER_MODE
- (BOOL)getQueryParam;

- (BOOL)readFromTagMemoryParam:(uint32_t)accessPassword
                           epc:(NSData*)epc
                    memoryBank:(uint8_t)memoryBank
                  startAddress:(uint16_t)startAddress
                    dataLength:(uint16_t)dataLength
                       timeout:(uint16_t)timeout;


- (BOOL)setReaderProgMode:(uint8_t)mode;


- (BOOL)writeToTagMemoryParam:(uint32_t)accessPassword
                          epc:(NSData*)epc
                   memoryBank:(uint8_t)memoryBank
                 startAddress:(uint16_t)startAddress
                  dataToWrite:(NSData*)dataToWrite
                      timeout:(uint16_t)timeout;

- (BOOL)blockwriteToTagMemory:(uint32_t)accessPassword
                          epc:(NSData*)epc
                   memoryBank:(uint8_t)memoryBank
                 startAddress:(uint16_t)startAddress
                  dataToWrite:(NSData*)dataToWrite;


- (BOOL)lockTagMemoryParam:(uint32_t)accessPassword
                       epc:(NSData*)epc
                  lockData:(uint32_t)lockData
                   timeout:(uint16_t)timeout;


- (BOOL)authenticate:(boolean_t)itemLevel
          iChallenge:(NSData*)iChallenge
              target:(NSData*)target;

- (BOOL)untraceable:(uint32_t)accessPassword
             target:(NSData*)target
              uFlag:(uint8_t)uFlag
                epc:(uint8_t)epc
                tid:(uint8_t)tid
               user:(uint8_t)user
              range:(uint8_t)range;

- (BOOL)readSignature:(uint32_t)accessPassword
               target:(NSData*)target
              pointer:(uint8_t)pointer
                count:(uint8_t)count;


- (BOOL)calGpAdc:(uint8_t)min max:(uint8_t)max;


- (BOOL)setAntiColMode:(uint8_t)mode
                startQ:(uint8_t)startQ
              maximumQ:(uint8_t)maximumQ
              minimumQ:(uint8_t)minimumQ;


- (BOOL)readFromTagMemory:(uint32_t)accessPassword
                      epc:(NSData*)epc
               memoryBank:(uint8_t)memoryBank
             startAddress:(uint16_t)startAddress
               dataLength:(uint16_t)dataLength;


- (BOOL)setQueryParam:(uint8_t)dr
                    m:(uint8_t)m
                trext:(uint8_t)trext
                  sel:(uint8_t)sel
              session:(uint8_t)session
               target:(uint8_t)target
                    q:(uint8_t)q;



- (BOOL)setRfCw:(uint8_t)on;



- (BOOL)getModulation;
- (BOOL)setModulation:(uint8_t)mode;


- (BOOL)eraseTagMemory1:(uint32_t)accessPassword
                    epc:(NSData*)epc
             memoryBank:(uint8_t)memoryBank
           startAddress:(uint16_t)startAddress
             dataLength:(uint16_t)dataLength;


- (BOOL)genericTrasport:(uint32_t)accessPassword
                    epc:(NSData *)epc
                     ts:(uint8_t)ts
                     rm:(uint8_t)rm
                     sz:(uint8_t)sz
                     gc:(NSData *)gc;


- (BOOL)getTemperature;
- (BOOL)eraseRegistry:(uint8_t)erase;
- (BOOL)getRegistryItem:(uint16_t)registryItem;


- (BOOL) setOptimumFrequencyHoppingTable;
- (BOOL) SetFrequencyHoppingMode:(uint8_t)mode;
- (BOOL) GetFrequencyHoppingMode;
- (BOOL) getStopCondition;
- (BOOL) setLeakageCalMode:(uint8_t)mode;
- (BOOL) setSmartHoppingOnOff:(BOOL)isOn;
#endif

@end

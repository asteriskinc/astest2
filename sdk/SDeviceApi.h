//
//  RcpApi.h


#import <Foundation/Foundation.h>
#import "protocols.h"


@class UartMgr;
@interface SDeviceApi : NSObject
//*******************************************************************************//
//  Common API (RFID, NFC, Barcode)
//  This funtion can use all device
//*******************************************************************************//
+ (NSString*)  getSDKVersion;
- (BOOL)open;
- (BOOL)isOpened;
- (BOOL)isConnected;
- (void)close;
- (BOOL)getRegion;
- (BOOL)getReaderInfomation;
- (BOOL)getReaderInfo:(uint8_t)infoType;
- (int) getCurrentBat;
- (BOOL)setRegion:(uint8_t)region;

- (BOOL)setBeep:(uint8_t)beepOn
   setVibration:(uint8_t)vibrationOn
setIllumination:(uint8_t)illuminationOn
setLED:(uint8_t)led;

- (int)setReaderPower:(BOOL)isOn
                buzzer:(BOOL)isBeep
             vibration:(BOOL)isVib
                   led:(BOOL)isLed
          illumination:(BOOL)isIllu
                  mode:(int)nDeviceType;

- (BOOL)setReaderProgMode:(uint8_t)mode;
- (void) setTagCount:(int)mtnu setSacnTime:(int)mtime setCycle:(int)repeatCycle;

@property (nonatomic, assign) BOOL isConnected;
@property (nonatomic, weak, setter = setDelegaCommon:) id<RcpCommonDelegate> delegateCommon;
@property (nonatomic, weak, setter = setDelegateRFID:) id<RcpRFIDDelegate> delegateRFID;
@property (nonatomic, weak, setter = setDelegateHWEvent:) id<HWEventDelegate> delegateHWEvent;

@end
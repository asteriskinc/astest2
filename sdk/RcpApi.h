//
//  RcpApi.h
#import <Foundation/Foundation.h>
#import "protocols.h"
@interface RcpApi : NSObject <UartMgrDelegate>
+ (id)        sharedInstance;
@property (nonatomic, weak) id<RcpCommonDelegate> delegateCommon;
@property (nonatomic, weak) id<RcpRFIDDelegate> delegateRFID;
@property (nonatomic, weak) id<HWEventDelegate> delegateHWEvent;
-(void) setJacketBarcodeConnecteMessage:(uint8_t)status;
@end





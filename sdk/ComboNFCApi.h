//  NFCApi.h
//  Created by Robin on 2016. 5. 11..

#import "SDeviceApi.h"
@interface ComboNFCApi : SDeviceApi
+ (id)  sharedInstance;
- (BOOL) sendRawData:(Byte*)sendData len:(int)nLen;
- (BOOL) setInventory:(float)fInventoryTime;
- (BOOL) startScan;
- (BOOL) stopScan;
@end

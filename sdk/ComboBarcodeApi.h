////  BarcodeApi.h
////  Created by Robin on 2016. 4. 22..

#import "SDeviceApi.h"

@interface ComboBarcodeApi : SDeviceApi
+ (id)  sharedInstance;
- (BOOL)startScan;
- (BOOL)stopScan;
- (void)setReaderType;
- (BOOL)setFactoryReset;
- (BOOL) sendComboBarcodeSettinfData:(Byte*)sendData len:(int)nLen;
@end

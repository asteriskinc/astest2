//  protocol0422.h
//  Created by Robin on 2016. 4. 22..

#ifndef protocol0422_h
#define protocol0422_h

#import "typeDefine.h"

@protocol UartMgrDelegate <NSObject>
@required
- (int)  receive:(NSData *)data;
- (void) plugStatusChanged:(NSInteger)status;
@end


/***********************************************************************/
#pragma mark HardWare Event Delegate
/***********************************************************************/

@protocol HWEventDelegate <NSObject>
@required

- (void)resPowerOnOff:(BOOL)isOn Device:(int)nDeviceType IsHWModeChange:(BOOL)bIsHWModeChange;

@optional
- (void)readerConnected:(uint8_t)status;
- (void)checkTriggerStatus:(NSString*)strStatus;
- (void)plugged:(BOOL)plug;
- (void)pushedTriggerButton;
- (void)releasedTriggerButton;
@end


/***********************************************************************/
#pragma mark Reader Common Delegate
/***********************************************************************/

@protocol RcpCommonDelegate <NSObject>
@optional


- (void)ackReceived:(uint8_t)commandCode;
- (void)errReceived:(NSData *)errCode;
- (void)startedReadScan:(uint8_t)status;
- (void)stopReadScan:(uint8_t)status;
- (void)adcReceived:(NSData*)data;
- (void)receivedScanData:(NSData *)readData DeviceType:(int)nDeviceType;
- (void)resFactoryRset:(uint8_t)status;

// ForTest :
- (void)nfcRawDataReceived:(NSData *)rawData;
- (void)barcodeRawDataReceived:(NSData *)rawData;
- (void)rfidRawDataReceived:(NSData *)rawData;
- (void)allRawDataReceived:(NSData *)rawData;
@end


/***********************************************************************/
#pragma mark Only RFID  Delegate
/***********************************************************************/
@protocol RcpRFIDDelegate <NSObject>
@optional

/////////////////* READ RFID DATA */
- (void)epcReceived:(NSData *)epc tid:(NSData *)tid;
- (void)pcEpcReceived:(NSData *)pcEpc;
- (void)pcEpcRssiReceived:(NSData *)pcEpc rssi:(int8_t)rssi;
- (void)rssiReceived:(uint16_t)rssi;

/////////////////* Settings Info */
- (void)didSetOutputPowerLevel:(uint8_t)status;
- (void)didSetChParamReceived:(uint8_t)statusCode;
- (void)responseResion:(uint8_t)status;
- (void)responseSetPwr:(uint8_t)status;
- (void)didSetAntiCol:(uint8_t)status;
- (void)didSetSession:(uint8_t)status; //Set Session
- (void)didSetFhLbt:(uint8_t)status; ////OnOff Time , Hopping Params
- (void)didSetBeep:(uint8_t)status;
- (void)didSetStopCon:(uint8_t)status;
- (void)channelReceived:(uint8_t)channel channelOffset:(uint8_t)channelOffset;
- (void)anticolParamReceived:(uint8_t)mode Counter:(uint8_t)counter;
- (void)modulationParamReceived:(uint8_t)param;
- (void)didSetOptiFreqHPTable:(uint8_t)status;
- (void)anticolParamReceived:(NSData *)param;
- (void)txPowerLevelReceived:(NSData*)power;
- (void)regionReceived:(uint8_t)region;
- (void)stopConditionsReceived:(NSData *)data;
- (void)didSetFreqHPTable:(uint8_t)status;
- (void)freqHPTableReceived:(uint8_t)status;
- (void)selectParamReceived:(NSData *)selParam;
- (void)queryParamReceived:(NSData *)qryParam;
- (void)fhLbtReceived:(NSData *)fhLb;
- (void)hoppingTableReceived:(NSData *)table;
- (void)didSetRegion:(uint8_t)status;
/*RF Module Version Update */
- (void)rfidModuleVersionReceived;
/* Beep,  RFID (Power, OnTime, OffTime) */
- (void)readerInfoReceived:(NSData *)data;

/* RFID RF On Off Time */
- (void)rfidOnOffTimeReceived:(NSData*)data;


/////////////////*RFID READ Write */
- (void)writedReceived:(uint8_t)statusCode;
- (void)sessionReceived:(uint8_t)session;

- (void)tagMemoryReceived:(NSData *)data;
- (void)killedReceived:(uint8_t)statusCode;//Kill Tag


- (void)lockedReceived:(uint8_t)statusCode;//LOCK Tag


- (void)registeryItemReceived:(NSData *)item;
- (void)genericReceived:(NSData*)data;
- (void)responseReboot:(uint8_t)status;
- (void)resSetLeakage:(uint8_t)status;


/////////////////* Registry*/
- (void)updatedRegistry:(uint8_t)statusCode;



/////////////////* FirmWare Down Load*/
- (void)responseFWData:(NSData *)FWData;


/////////////////* ETC */
- (void)engineerReceived:(NSData *)pcEpc;
- (void)engineerAdc:(NSData*)data;
- (void)tempReceived:(uint8_t)temp;

@end

#endif
